import random

from sc2.ids.unit_typeid import UnitTypeId
from sc2.ids.ability_id import AbilityId
from sc2.constants import *
from sc2.unit import Unit

from .build_order_id import BuildOrderId


class MyHatchery():
    def __init__(self):
        self.tag = None
        self.name = None
        self.position = None


#class MyExtractor():
    #def __init__(self):
        #self.tag = None
        #self.name = None
        #self.home = None
        #self.away = None
        #self.position = None
        #self.assigned = None
        #self.assigned_position = None


class MyDrone():
    def __init__(self):
        self.tag = None
        self.name = None
        self.position = None
        self.assigned = None
        self.assigned_position = None


class Resourcess():
    def __init__(self, bot=None):
#        print("resources.py init")
        self.bot = bot
        self.bot.neededworkers = 0
        self.bot.drone_count = 12
        self.bot.hatchery_count = 1
        self.bot.myDrones = []
        self.bot.myHatcheries = []
        self.bot.idle_worker_count = 0

        self.initial_update_flag = True
        self.maxworkers = 0
        self.assigned_harvesters = 0
        self.moved_workers_to_gas = False
        self.moved_workers_from_gas = False
        self.moved_workers_to_gas_1 = False
        self.moved_workers_to_gas_2 = False
        # self.mboost_started = False

    async def Run(self):
        await self.unit_update()
        await self.queen_inject()
        await self.change_worker_rally_point()
        await self.manageWorkers()
        await self.move_workers_to_first_gas()
        await self.move_workers_from_first_gas()
        self.manage_gas_workers()
        #await self.move_workers_to_more_gas()

    async def unit_update(self):
        if self.initial_update_flag:
            await self.initial_update()
            self.initial_update_flag = False
        else:
            await self.hatch_update()

        if False: #testing TLL 2020-04-24
            await self.data_dump()


    async def initial_update(self):
        for h in self.bot.hatcheries:
            unitHatchery = MyHatchery()
            unitHatchery.tag = h.tag
            unitHatchery.position = self.bot.hatchery.position
            self.bot.myHatcheries.append(unitHatchery)

        for drone in self.bot.units(DRONE):
            unitDrone = MyDrone()
            unitDrone.tag = drone.tag
            unitDrone.assigned = 1
            unitDrone.assigned_position = self.bot.hatchery.position
            self.bot.myDrones.append(unitDrone)

#        for unit in self.bot.myHatcheries:
#            print(unit.tag, " ", unit.name, " ", unit.position)

#        for unit in self.bot.myDrones:
#            print(unit.tag, " ", unit.name, " ", unit.position, end = " ")
#            print(unit.assigned, " ", unit.assigned_position)

    async def hatch_update(self):
        for h in (self.bot.hatcheries | self.bot.units(LAIR)
                | self.bot.units(HIVE)):
            self.bot.myHatcheries.append(h)

    async def data_dump(self):
        # TLL strictly for testing
        flag = False
        for unit in self.bot.units:
            print(unit.tag, " ", unit.name, " ", unit.position, end = " ")
            print(unit.mineral_contents, " ", unit.vespene_contents, end= " ")
            print(unit.is_using_ability, " ", unit.owner_id)
            if flag:
                for action in dir(unit):
                    print(action)
                flag = False

        #for s in self.bot.structures:
        #    for action in dir(s):
        #        print(action)
        #    quit()

        quit()

    async def queen_inject(self):
        for queen in self.bot.units(QUEEN).idle:
            abilities = await self.bot.get_available_abilities(queen)
            if AbilityId.EFFECT_INJECTLARVA in abilities:
                h = (self.bot.hatcheries | self.bot.structures(LAIR) |
                    self.bot.structures(HIVE))
                if h.exists:
                    inject_pos = h.closest_to(queen)
                    queen(EFFECT_INJECTLARVA, inject_pos)
#                self.bot.write_data.print_inject()

    async def change_worker_rally_point(self):
        for th in self.bot.zerg_towns:
            needed_miners = th.ideal_harvesters - th.assigned_harvesters
            if needed_miners > 0:
                mine_to = self.bot.mineral_field.closest_to(th)
                th(RALLY_HATCHERY_WORKERS, mine_to)

        # new hatchery completed
        hatch_count = (self.bot.hatcheries.amount +
                self.bot.structures(LAIR).amount +
                self.bot.structures(HIVE).amount)
        if  self.bot.hatchery_count < hatch_count:
            mine_to = None
            print(hatch_count, self.bot.hatchery_count)
            max_needed_miners = 0
            for th in self.bot.zerg_towns:
                needed_miners = th.ideal_harvesters - th.assigned_harvesters
                print(f"Needed miners: {needed_miners}")
                if needed_miners > 0:
                    mine_to = self.bot.mineral_field.closest_to(th)
                    th(RALLY_HATCHERY_WORKERS, mine_to)
                if max_needed_miners < needed_miners:
                    mine_to = self.bot.mineral_field.closest_to(th)
                    max_needed_miners = needed_miners
            if mine_to == None:
                return
            for th in self.bot.zerg_towns:
                th(RALLY_HATCHERY_UNITS, self.bot.rushwait_location)
                needed_miners = th.ideal_harvesters - th.assigned_harvesters
                if (max_needed_miners == needed_miners or needed_miners <= 0):
                    # mine_to = self.bot.mineral_field.closest_to(th)
                    th(RALLY_HATCHERY_WORKERS, mine_to)

            self.bot.hatchery_count = hatch_count
#            quit()

    async def manageWorkers(self):
        # Distribute workers
        if len(self.bot.workers) == 0:
            return

        self.manage_idle_workers()

        if len(self.bot.zerg_towns) > 1:
            self.worker_migration()

    def manage_idle_workers(self):
        for drone in self.bot.workers:
            if drone.is_idle:
                for th in self.bot.zerg_towns:
                    if th.assigned_harvesters < th.ideal_harvesters:
                        mine_to = self.bot.mineral_field.closest_to(th)
                        drone.gather(mine_to, queue=True)

    def worker_migration(self):
        for th in self.bot.zerg_towns:
            if th.assigned_harvesters > th.ideal_harvesters:
                # This logic commented out because it's broken
                # drone = self.bot.workers.closer_than(15, th).random
                drones = self.bot.units.filter(lambda d:
                    d.type_id == DRONE
                    and d.distance_to(th) < 8
                    # and d.tag in self.bot.mineral_drone_tags
                    and not d.is_carrying_resource
                )

                if len(drones) > 0:
                    drone = drones.random
                else:
                    continue

                for move_to in (self.bot.hatcheries |
                    self.bot.structures(LAIR) | self.bot.structures(HIVE)
                ):
                    if move_to.assigned_harvesters < move_to.ideal_harvesters:
                        mine_to = self.bot.mineral_field.closest_to(move_to)
                        drone.gather(mine_to)

    async def move_workers_to_first_gas(self):
        if self.bot.gas_buildings.ready.exists and not self.moved_workers_to_gas:
            self.moved_workers_to_gas = True
            extractor = self.bot.gas_buildings.first
            drones = self.bot.units.filter(
                    lambda unit: unit.type_id == UnitTypeId.DRONE
                    and not unit.is_carrying_resource)

            for drone in drones:
                if (drone.distance_to(extractor) < 9
                    and drone.tag in self.bot.mineral_drone_tags
                ):
                    drone.gather(extractor)
                    self.bot.vespene_drone_tags.add(drone.tag)
                    self.bot.mineral_drone_tags.remove(drone.tag)
                    if len(self.bot.vespene_drone_tags) > 2:
                        break

    async def move_workers_from_first_gas(self):
        if self.moved_workers_from_gas or not self.bot.mboost_started:
            return

        if len(self.bot.vespene_drone_tags) == 0:
            print("vespene workers moved")
            self.moved_workers_from_gas = True
            return
        elif self.bot.build_order == BuildOrderId.BUILD_RCF:
            self.moved_workers_from_gas = True
            return

        for drone in self.bot.units(DRONE):
            if (drone.tag in self.bot.vespene_drone_tags and not
                drone.is_carrying_resource
            ):
                m = self.bot.mineral_field.closest_to(drone.position)
                if m:
                    drone.gather(m)
                    self.bot.mineral_drone_tags.add(drone.tag)
                    self.bot.vespene_drone_tags.remove(drone.tag)

    def manage_gas_workers(self):
        if (self.bot.vespene < 100
        or self.bot.vespene < .30 * self.bot.minerals):
            self.move_workers_to_more_gas()
        elif (self.bot.vespene >= 300
        and self.bot.vespene > .50 * self.bot.minerals):
            self.move_workers_to_more_minerals()

    #async def move_workers_to_more_gas(self):
    def move_workers_to_more_gas(self):
        if ((self.moved_workers_to_gas and
            not self.moved_workers_from_gas) and
            len(self.bot.gas_buildings) <= 1
        ):
            return
        elif (self.bot.zergling_rush and
            self.bot.time < 300
        ):
            return
        elif (self.bot.build_order == BuildOrderId.ZVT_ANTI_P_1 and
            self.bot.time < 210
        ):
            return

        for gb in self.bot.gas_buildings:
            if gb.assigned_harvesters < gb.ideal_harvesters:
                self.pick_drone_mine_gas(gb)

    def pick_drone_mine_gas(self, gb):
        for drone in self.bot.units(DRONE):
            if (drone.distance_to(gb) < 15 and not
                drone.is_carrying_resource and
                drone.tag in self.bot.mineral_drone_tags
            ):
                drone.gather(gb)
                # comment out - this broke something 2020/11/20
                # self.bot.vespene_drone_tags.add(drone.tag)
                # self.bot.mineral_drone_tags.remove(drone.tag)
                return

    def move_workers_to_more_minerals(self):
        for th in self.bot.zerg_towns:
            if th.assigned_harvesters < th.ideal_harvesters:
                drones = self.bot.workers.closer_than(15, th)
                for drone in drones:
                    if drone.is_carrying_resource:
                        continue
                    else:
                        m = self.bot.mineral_field.closer_than(10, drone.position)
                        if m.exists:
                            drone.gather(m.random, queue=True)
                            break
