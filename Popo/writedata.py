import time, math
from sc2.client import *
from sc2.constants import *

class WriteData():
    def __init__(self, bot=None):
        self.bot = bot
        file_dir='data/'
        file_name = 'PP2-'+time.strftime("%Y-%m-%d-%H-%M-%S")+'.txt'
        self.output_file = file_dir + file_name

    def print_start(self):
        self.file = open(self.output_file, "w")
        self.file.write(str(self.output_file))
        self.file.write("\n")
        self.print_name()
        self.print_map_info()

    def print_end(self):
        self.file.close()

    def print_name(self):
        self.file.write(f"Name: {self.bot.NAME}\n")

    def print_map_info(self):
        self.file.write(f"Map: {self.bot.game_info.map_name} ")
        self.file.write("\n")

    def print_opponent_info(self):
        self.file.write(f"Opponent: {self.bot.opponent_id}\n")
        self.file.write(f"Opponent: {self.bot.opponent_name}\n")
        self.file.write(f"Race: {self.bot.race_enemy}")
        self.file.write("\n")

    def print_build_order(self):
        self.file.write(f"Build order: {self.bot.build_order_label}\n")
        self.file.flush()

    def print_tie(self):
        self.print_time()
        self.file.write(" Result: Expect tie\n")
        self.print_clock_time()
        self.file.write("\n")
        self.file.flush()

    def print_loss(self):
        self.print_time()
        self.file.write(" Result: Expect loss\n")
        self.print_clock_time()
        self.file.write("\n")
        self.file.flush()

    def print_result(self, game_result):
        #if self.expected_result == 1:
        #    game_result = "Win"
        #elif self.expected_result == 0:
        #    game_result = "Tie"
        #else:
        #    game_result = "Loss"
        self.print_time()
        self.file.write(f"\nResult: {game_result}\n")
        self.print_clock_time()
        self.file.write("\n")
        self.file.flush()

    def print_clock_time(self):
        out_line = time.strftime("%Y-%m-%d-%H-%M-%S")
        self.file.write(out_line)

    def print_time(self):
        duration = self.bot.time
        minutes = str(math.floor(duration / 60))
        seconds = str(round(duration % 60)).zfill(2)
        out_line = "time: "+ minutes + ":" + seconds
        self.file.write(out_line)

    def print_short_status(self):
        self.print_name()
        self.print_time()
        self.print_minerals()
        self.print_vespene()

    def print_status(self):
        self.print_short_status()
        self.print_larvae()
        self.print_eggs()
        self.print_drones()
        self.print_overlords()
        self.print_zerglings()
        self.print_banelings()
        self.print_roaches()
        self.print_queens()
        self.print_hatcheries()
        self.print_lairs()
        self.print_extractors()
        self.print_inf_pits()
        self.file.write("\n")

    def print_war_info(self):
        self.print_name()
        self.print_time()
        self.print_war_status()
        self.print_enemy_attackers()
        self.file.write("\n")

    def print_war_status(self):
        war_status = " war status: " + self.war_status
        self.file.write(war_status)

    def print_enemy_attackers(self):
        enemy_info = " enemy_attackers: " + str(self.enemy_attacker_count)
        self.file.write(enemy_info)

    def print_inject(self):
        self.print_short_status()
        self.print_larvae()
        self.file.write(" inject\n")

    def move_workers_to_gas(self):
        self.print_short_status()
        self.file.write(" move workers to gas\n")

    def print_boost(self):
        self.print_short_status()
        self.file.write(" start metabolic boost\n")

    def train_drone(self):
        self.print_short_status()
        self.file.write(" train drone\n")

    def start_overlord(self):
        self.print_short_status()
        self.file.write(" start Overlord\n")

    def start_zerglings(self):
        self.print_short_status()
        self.print_larvae()
        self.file.write(" start Zerglings\n")

    def start_banelings(self):
        self.print_short_status()
        self.print_larvae()
        self.file.write(" start Banelings\n")

    def start_roach(self):
        self.print_short_status()
        self.print_larvae()
        self.file.write(" start Roach\n")

    def print_minerals(self):
        mineral_info = " minerals: " + str(self.bot.minerals)
        self.file.write(mineral_info)

    def print_vespene(self):
        vespene_info = " vespene: " + str(self.bot.vespene)
        self.file.write(vespene_info)

    def print_larvae(self):
        larvae_info = " larvae: " + str(len(self.bot.units(LARVA)))
        self.file.write(larvae_info)

    def print_eggs(self):
        egg_info = " eggs: " + str(len(self.bot.units(EGG)))
        self.file.write(egg_info)

    def print_drones(self):
        drone_info = " drones: " + str(len(self.bot.units(DRONE)))
        self.file.write(drone_info)

    def print_overlords(self):
        self.file.write(" overlords: " + str(len(self.bot.units(OVERLORD))))

    def print_zerglings(self):
        zergling_info = " zerglings: " + str(len(self.bot.units(ZERGLING)))
        self.file.write(zergling_info)

    def print_banelings(self):
        baneling_info = " banelings: " + str(len(self.bot.units(BANELING)))
        self.file.write(baneling_info)

    def print_roaches(self):
        roach_info = " roaches: " + str(len(self.bot.units(ROACH)))
        self.file.write(roach_info)

    def print_queens(self):
        queen_info = " queens: " + str(len(self.bot.units(QUEEN)))
        self.file.write(queen_info)

    def print_hatcheries(self):
        hatchery_info = " hatcheries: " + str(len(self.bot.units(HATCHERY)))
        self.file.write(hatchery_info)

    def print_lairs(self):
        lair_info = " lairs: " + str(len(self.bot.units(LAIR).ready))
        self.file.write(lair_info)

    def print_extractors(self):
        extractor_info = " extractors: " + str(len(self.bot.units(EXTRACTOR).ready))
        self.file.write(extractor_info)

    def print_inf_pits(self):
        inf_pit_info = " infestation pits: " + str(len(self.bot.units(INFESTATIONPIT).ready))
        self.file.write(inf_pit_info)

    def start_extractor(self):
        self.print_short_status()
        self.file.write(" start extractor\n")

    def start_spawning_pool(self):
        self.print_short_status()
        self.file.write(" start spawning pool\n")

    def start_queen(self):
        self.print_short_status()
        self.file.write(" start queen\n")

    def start_hatchery(self):
        self.print_short_status()
        self.file.write(" start hatchery\n")

    def start_baneling_nest(self):
        self.print_short_status()
        self.file.write(" start baneling nest\n")

    def start_roach_warren(self):
        self.print_short_status()
        self.file.write(" start roach warren\n")

    def start_lair(self):
        self.print_short_status()
        self.file.write(" start lair\n")

    def start_evo_chamber(self):
        self.print_short_status()
        self.file.write(" start evolution chamber\n")

    def start_inf_pit(self):
        self.print_short_status()
        self.file.write(" start infestation pit\n")

    def start_spine_crawler(self):
        self.print_short_status()
        self.file.write(" start spine crawler\n")

    def start_spore_crawler(self):
        self.print_short_status()
        self.file.write(" start spore crawler\n")

    def start_dropper_lord(self):
        self.print_short_status()
        self.file.write(" start dropper lord\n")

    def start_nydus_network(self):
        self.print_short_status()
        self.file.write(" start nydus network\n")

    def start_swarm_host(self):
        self.print_short_status()
        self.file.write(" start swarm host\n")

    def print_spawn_locusts(self):
        self.print_short_status()
        self.file.write(" spawn locusts\n")
