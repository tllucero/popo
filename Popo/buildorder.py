import math
import random
import enum

from sc2.client import *
from sc2.constants import *
from sc2 import Race

from .build_order_id import BuildOrderId

class BuildId(enum.Enum):
    NOBOOST = 5001
    BOOST = 5002
    NOZERGLING = 5003
    NOEVO = 5004
    EVO = 5005


class BuildOrder():
    def __init__(self, bot=None):
        self.bot = bot

        self.initial_update_flag = True

        self.build_list = []
        self.bot.build_order_label = None
        self.bot.build_order_list = []
        self.bot.build_order_list_all = []
        list(self.bot.build_order_list)

        self.bot.build_upgrade_list = []
        list(self.bot.build_upgrade_list)

        for item in BuildId:
            assert not item.name in globals()
            globals()[item.name] = item

        # dev log level here. For production, mod popo.py
        self.build_log_level = True
        self.build_log_level_extractor = True
        self.build_log_level_hatch = False
        self.build_log_level_ultra = True


        self.hatchery_bounces = 0
        self.extractor_started = False
        self.spawning_pool_started = False
        self.boost_flag = True
        self.zergling_flag = True
        self.evo_flag = True
        self.bot.mboost_started = False
        self.bot.baneling_speed_started = False
        self.bot.adrenal_glands_started = False
        self.bot.roach_speed_started = False
        self.hatch_count = 1
        self.ideal_mineral_harvesters = 0
        self.ideal_vespene_harvesters = 0
        self.assigned_mineral_harvesters = 0
        self.assigned_vespene_harvesters = 0
        self.x_sign = None
        self.y_sign = None
        self.r = None
        self.target = None
        self.spire = None
        self.d = None

        # build details
        self.build_infestation_pit = False
        self.build_hive = False

        # crackling build delays this
        self.build_roach_warren = True
        self.build_hydralisk_den = True

        self.bot.extractors = 0
        self.bot.extractor_time = 999999
        self.bot.attack_power = 0


    async def read_the_order(self):
        if self.bot.build_order == BuildOrderId.TWELVE_POOL:
            self.build_order_12_pool()
        elif self.bot.build_order == BuildOrderId.TWELVE_POOL_2:
            self.build_order_12_pool_2()
        elif self.bot.build_order == BuildOrderId.TWELVE_POOL_3:
            self.build_order_12_pool_3()
        elif self.bot.build_order == BuildOrderId.ZERGLING_RUSH:
            self.build_order_zergling_rush()
        elif self.bot.build_order == BuildOrderId.DROPPER_LORD_14:
            self.build_order_14_dropper_lord()
        elif self.bot.build_order == BuildOrderId.BLIZZCON_19:
            self.build_order_blizzcon_19()
        elif self.bot.build_order == BuildOrderId.PIG_ELEVATOR:
            self.build_order_pig_elevator()
        elif self.bot.build_order == BuildOrderId.PIG_SWARM:
            self.build_order_pig_swarm()
        elif self.bot.build_order == BuildOrderId.TL_SWARM:
            self.build_order_tl_swarm()
        elif self.bot.build_order == BuildOrderId.HATCH_16:
            self.build_order_16()
        elif self.bot.build_order == BuildOrderId.BUILD_17_16_17:
            self.build_order_17()
        elif self.bot.build_order == BuildOrderId.BUILD_ECONO_01:
            self.build_order_econo_01()
        elif self.bot.build_order == BuildOrderId.BUILD_ECONO_02:
            self.build_order_econo_02 ()
        elif self.bot.build_order == BuildOrderId.BUILD_ECONO_DB:
            self.build_order_econo_db ()
        elif self.bot.build_order == BuildOrderId.BUILD_RCF:
            self.build_order_rcf()
        elif self.bot.build_order == BuildOrderId.FAST_BANELING:
            self.build_order_fast_baneling()
        elif self.bot.build_order == BuildOrderId.ONE_BASE_FAST_ROACH:
            self.build_order_one_base_fast_roach()
        elif self.bot.build_order == BuildOrderId.ONE_BASE_FAST_ROACH_02:
            self.build_order_one_base_fast_roach_02()
        elif self.bot.build_order == BuildOrderId.ZVZ_STD_1:
            self.build_order_zvz_standard_1()
        elif self.bot.build_order == BuildOrderId.ONE_BASE_ROACH_01:
            self.build_order_one_base_roach_01()
        elif self.bot.build_order == BuildOrderId.TWO_BASE_ROACH_01:
            self.build_order_two_base_roach_01()
        elif self.bot.build_order == BuildOrderId.TWO_BASE_ROACH_02:
            self.build_order_two_base_roach_02()
        elif self.bot.build_order == BuildOrderId.ZVZ_LONG:
            self.build_order_zvz_long()
        elif self.bot.build_order == BuildOrderId.THREE_BASE_ROACH_01:
            self.build_order_three_base_roach_01()
        elif self.bot.build_order == BuildOrderId.ONE_BASE_HYDRA:
            self.build_order_1_hydra()
        elif self.bot.build_order == BuildOrderId.TWO_BASE_HYDRA:
            self.build_order_2_hydra()
        elif self.bot.build_order == BuildOrderId.FAST_FOUR_HATCHERY:
            self.build_order_fast_four_hatchery()
        elif self.bot.build_order == BuildOrderId.ONE_BASE_BROODLORD:
            self.build_order_1_broodlord()
        elif self.bot.build_order == BuildOrderId.TWO_BASE_BROODLORD:
            self.build_order_2_broodlord()
        elif self.bot.build_order == BuildOrderId.ZVT_STD_1:
            self.build_order_zvt_standard_1()
        elif self.bot.build_order == BuildOrderId.ZVT_STD_2:
            self.build_order_zvt_standard_2()
        elif self.bot.build_order == BuildOrderId.ZVT_ANTI_P_1:
            self.build_order_zvt_anti_p_1()
        elif self.bot.build_order == BuildOrderId.ZVT_ANTI_A_1:
            self.build_order_zvt_anti_a_1()
        elif self.bot.build_order == BuildOrderId.QUEEN_21:
            self.build_order_queen_21()
        elif self.bot.build_order == BuildOrderId.CRACKLING:
            self.build_order_crackling()
        elif self.bot.build_order == BuildOrderId.BANELING_BUST:
            self.build_order_baneling_bust()
        elif self.bot.build_order == BuildOrderId.DEFEND_ZERGLING_RUSH:
            self.build_order_defend_zergling_rush()
        elif self.bot.build_order == BuildOrderId.DEFEND_PROXY_GATEWAY:
            self.build_order_defend_proxy_gateway()
        elif self.bot.build_order == BuildOrderId.DEFEND_PROXY_GATEWAY_02:
            self.build_order_defend_proxy_gateway_02()
        elif self.bot.build_order == BuildOrderId.DEFEND_CANNON_RUSH:
            self.build_order_defend_cannon_rush()
        elif self.bot.build_order == BuildOrderId.DEFEND_CANNON_RUSH_02:
            self.build_order_defend_cannon_rush_02()
        elif self.bot.build_order == BuildOrderId.WORKER_RUSH:
            self.build_order_worker_rush()
        elif self.bot.build_order == BuildOrderId.TEST_SCOUT_TIMING:
            self.build_order_test_scout_timing()
        elif self.bot.build_order == BuildOrderId.TEST_CANCEL_HATCH:
            self.build_order_test_cancel_hatch()
        else:
            self.build_order_short()

    def build_order_short(self):
        self.bot.build_order_label = "short"
        self.bot.build_order_list = [DRONE]

    def build_order_12_pool(self):
        self.bot.build_order_label = "12 pool"
        self.bot.build_order_list = [SPAWNINGPOOL, ZERGLING, ZERGLING,
        ZERGLING, OVERLORD, ZERGLING, ZERGLING, ZERGLING, ZERGLING,
        ZERGLING, ZERGLING, ZERGLING, ZERGLING, OVERLORD]
        self.melee_first_upgrades()

    def build_order_12_pool_2(self):
        self.bot.build_order_label = "12 pool 2"
        self.bot.build_order_list = [SPAWNINGPOOL, OVERLORD, DRONE,
        ZERGLING, ZERGLING, ZERGLING, QUEEN, ZERGLING, ZERGLING, ZERGLING,
        OVERLORD, ZERGLING, ZERGLING, ZERGLING, ZERGLING, ZERGLING,
        ZERGLING, ZERGLING, ZERGLING, OVERLORD, ZERGLING, ZERGLING]
        self.melee_first_upgrades()

    def build_order_12_pool_3(self):
        self.bot.build_order_label = "12 pool 3"
        self.bot.build_order_list = [SPAWNINGPOOL, DRONE, DRONE, OVERLORD,
        DRONE, ZERGLING, ZERGLING, ZERGLING, QUEEN, DRONE, EXTRACTOR, DRONE,
        HATCHERY, OVERLORD, DRONE, QUEEN, ZERGLING, ZERGLING,
        ZERGLING, OVERLORD, ZERGLING, ZERGLING, ZERGLING, ZERGLING, ZERGLING,
        ZERGLING, ZERGLING, ZERGLING, OVERLORD, ZERGLING, ZERGLING]
        self.melee_first_upgrades()

    def build_order_zergling_rush(self):
        self.bot.build_order_label = "zergling rush"
        self.bot.build_order_list = [DRONE, DRONE, EXTRACTOR,
            SPAWNINGPOOL, DRONE, OVERLORD, ZERGLING, ZERGLING, ZERGLING,
            ZERGLING, ZERGLING, OVERLORD, ZERGLING, ZERGLING]
        self.melee_first_upgrades()

    def build_order_14_dropper_lord(self):
        self.bot.build_order_label = "14 dropper lord"
        self.bot.build_order_list = [NOBOOST, DRONE, DRONE, EXTRACTOR,
            SPAWNINGPOOL, DRONE, LAIR, OVERLORD, ZERGLING, ZERGLING, ZERGLING,
            ZERGLING, ZERGLING, MORPH_OVERLORDTRANSPORT, OVERLORD, ZERGLING, ZERGLING]
        self.melee_first_upgrades()

    def build_order_blizzcon_19(self):
        self.bot.build_order_label = "blizzcon 19 18"
        self.bot.build_order_list = [DRONE, OVERLORD, DRONE, DRONE, DRONE,
            DRONE, DRONE, DRONE, EXTRACTOR, SPAWNINGPOOL, DRONE]

    def build_order_pig_elevator(self):
        self.bot.build_order_label = "pig elevator"
        self.bot.build_order_list = [NOBOOST, DRONE, OVERLORD, DRONE, DRONE,
            DRONE, DRONE, DRONE, DRONE, HATCHERY, EXTRACTOR, SPAWNINGPOOL,
            DRONE, DRONE, DRONE, OVERLORD, DRONE, DRONE, QUEEN, ZERGLING,
            LAIR, ZERGLING, DRONE, DRONE, DRONE, QUEEN, DRONE, HATCHERY,
            OVERLORD, DRONE, BOOST, ZERGLING, ZERGLING, DRONE, DRONE,
            QUEEN, OVERLORD, OVERLORD, EXTRACTOR, EVOLUTIONCHAMBER,
            ZERGLING, ZERGLING, OVERLORD, DRONE, DRONE, ZERGLING,
            ZERGLING, ZERGLING, ZERGLING, ZERGLING, ZERGLING, ZERGLING,
            ZERGLING, OVERLORD, DRONE, DRONE, EXTRACTOR, DRONE, QUEEN,
            EVOLUTIONCHAMBER, ZERGLING, ZERGLING, ZERGLING, OVERLORD, EXTRACTOR]
        self.melee_first_upgrades()

    def build_order_pig_swarm(self):
        self.bot.build_order_label = "pig swarm"
        self.bot.build_order_list = [NOBOOST, DRONE, OVERLORD, DRONE, DRONE,
            DRONE, HATCHERY, DRONE, DRONE, EXTRACTOR, SPAWNINGPOOL,
            DRONE, DRONE, DRONE, DRONE, QUEEN, ZERGLING, OVERLORD, QUEEN,
            ZERGLING, DRONE, DRONE, DRONE, LAIR, QUEEN, DRONE, DRONE,
            DRONE, DRONE, OVERLORD, DRONE, DRONE,
            EXTRACTOR, ZERGLING, DRONE, BOOST, DRONE, EXTRACTOR,
            INFESTATIONPIT, EXTRACTOR, DRONE, DRONE, SPORECRAWLER,
            HATCHERY, DRONE, DRONE, DRONE, DRONE]

    def build_order_tl_swarm(self):
        self.bot.build_order_label = "tl swarm"
        self.bot.build_order_list = [NOBOOST, DRONE, DRONE, EXTRACTOR,
            SPAWNINGPOOL, DRONE, OVERLORD, DRONE, DRONE, DRONE, LAIR,
            EXTRACTOR, DRONE, DRONE, DRONE, DRONE, DRONE,
            INFESTATIONPIT, OVERLORD, QUEEN, DRONE, DRONE,
            HATCHERY, SWARMHOSTMP, SWARMHOSTMP, SWARMHOSTMP, OVERLORD,
            SWARMHOSTMP, NYDUSNETWORK, DRONE, DRONE, DRONE, DRONE,
            OVERLORD, SWARMHOSTMP, SWARMHOSTMP]

    def build_order_16(self):
        self.bot.build_order_label = "16 hj"
        self.bot.build_order_list = [NOBOOST, DRONE, OVERLORD, DRONE, DRONE,
            DRONE, HATCHERY, DRONE, DRONE, DRONE, EXTRACTOR, SPAWNINGPOOL,
            DRONE, DRONE, DRONE, DRONE, DRONE, ZERGLING, QUEEN, BOOST, OVERLORD]

    def build_order_17(self):
        self.bot.build_order_label = "17 sh"
        self.bot.build_order_list = [NOBOOST, DRONE, OVERLORD, DRONE, DRONE,
            DRONE, DRONE, EXTRACTOR, SPAWNINGPOOL, DRONE, DRONE, HATCHERY,
            DRONE, ZERGLING, QUEEN, BOOST, DRONE, ROACHWARREN, DRONE, DRONE,
            OVERLORD, ZERGLING, ROACH, ROACH, ROACH, ROACH, OVERLORD,
            ROACH, ROACH, ZERGLING, ZERGLING, ZERGLING, ZERGLING, ZERGLING,
            ZERGLING, OVERLORD, ZERGLING, ZERGLING]

    def build_order_econo_01(self):
        self.bot.build_order_label = "econo 1"
        self.bot.build_order_list = [DRONE, OVERLORD, DRONE, DRONE, DRONE,
            DRONE, DRONE, DRONE, EXTRACTOR, SPAWNINGPOOL, DRONE,
            DRONE, DRONE, OVERLORD]

    def build_order_econo_02(self):
        self.bot.build_order_label = "econo 2"
        self.bot.build_order_list = [DRONE, OVERLORD, DRONE, DRONE, DRONE,
            DRONE, HATCHERY, DRONE, EXTRACTOR, DRONE, SPAWNINGPOOL, DRONE,
            DRONE, DRONE, OVERLORD]

    def build_order_econo_db(self):
        self.bot.build_order_label = "econo db"
        self.bot.build_order_list = [NOBOOST, DRONE, OVERLORD, DRONE, DRONE,
        DRONE, HATCHERY, DRONE, DRONE, DRONE, EXTRACTOR, DRONE, SPAWNINGPOOL,
        DRONE, DRONE, DRONE, OVERLORD, HATCHERY, DRONE, DRONE, DRONE,
        QUEEN, QUEEN, OVERLORD, DRONE, DRONE, DRONE, DRONE, DRONE,
        QUEEN, DRONE, DRONE, BOOST, ZERGLING, QUEEN, OVERLORD, DRONE,
        DRONE, DRONE, ZERGLING, OVERLORD, OVERLORD, ZERGLING, ZERGLING,
        ZERGLING, DRONE, DRONE, DRONE, DRONE, SPORECRAWLER, QUEEN,
        DRONE, DRONE, DRONE]

    def build_order_rcf(self):
        self.bot.build_order_label = "rcf"
        self.bot.build_order_list = [DRONE, OVERLORD, DRONE, DRONE, HATCHERY,
            DRONE, DRONE, DRONE, EXTRACTOR, SPAWNINGPOOL, DRONE, DRONE,
            DRONE, DRONE, OVERLORD, ZERGLING, QUEEN, QUEEN, DRONE, DRONE,
            DRONE, DRONE, HATCHERY, DRONE, DRONE, OVERLORD, DRONE, DRONE,
            DRONE, QUEEN, ZERGLING, DRONE, DRONE, OVERLORD, DRONE, DRONE,
            DRONE, OVERLORD, DRONE, DRONE, BANELINGNEST ]

    def build_order_fast_baneling(self):
        self.bot.build_order_label = "fast baneling"
        self.bot.build_order_list = [NOBOOST, DRONE, OVERLORD, DRONE,
            DRONE, DRONE, DRONE, EXTRACTOR, SPAWNINGPOOL, DRONE, DRONE, DRONE,
            DRONE, DRONE, BANELINGNEST, QUEEN, OVERLORD, DRONE,
            ZERGLING, ZERGLING, ZERGLING, ZERGLING, ZERGLING, ZERGLING, OVERLORD,
            ZERGLING, ZERGLING, ZERGLING, ZERGLING, ZERGLING, ZERGLING,
            ZERGLING, ZERGLING, OVERLORD]
        self.melee_first_upgrades()

    def build_order_one_base_fast_roach(self):
        self.bot.build_order_label = "one base fast roach"
        self.bot.build_order_list = [NOBOOST, DRONE, OVERLORD, DRONE,
            EXTRACTOR, SPAWNINGPOOL, DRONE, DRONE, DRONE, EXTRACTOR,
            ROACHWARREN,  DRONE, DRONE, ROACH, ROACH, ROACH, ROACH,
            OVERLORD, ROACH, ROACH, ROACH, ROACH, OVERLORD, ROACH,
            ROACH, ROACH, ROACH, OVERLORD]

    def build_order_one_base_fast_roach_02(self):
        self.bot.build_order_label = "one base fast roach 02"
        self.bot.build_order_list = [NOBOOST, DRONE, OVERLORD, DRONE,
            DRONE, DRONE, DRONE, EXTRACTOR, SPAWNINGPOOL, DRONE, DRONE, DRONE,
            DRONE, DRONE, ROACHWARREN, QUEEN, OVERLORD, DRONE,
            ROACH, ROACH, OVERLORD, QUEEN,
            ROACH, ROACH, OVERLORD,
            ROACH, ROACH, ROACH, ROACH, OVERLORD,
            ROACH, ROACH, ROACH, ROACH, OVERLORD, EXTRACTOR]

    def build_order_zvz_standard_1(self):
        self.bot.build_order_label = "zvz std 1"
        self.bot.build_order_list = [NOBOOST, DRONE, OVERLORD, DRONE,
            DRONE, DRONE, DRONE, EXTRACTOR, SPAWNINGPOOL, DRONE, DRONE, DRONE,
            DRONE, DRONE]

    def build_order_one_base_roach_01(self):
        self.bot.build_order_label = "one base roach 01"
        self.bot.build_order_list = [DRONE, OVERLORD, DRONE, DRONE, DRONE,
            DRONE, EXTRACTOR, DRONE, SPAWNINGPOOL, DRONE, DRONE, DRONE,
            DRONE, OVERLORD, EXTRACTOR, DRONE, DRONE, DRONE, QUEEN,
            ROACHWARREN, ZERGLING, ZERGLING, DRONE, DRONE,
            DRONE, DRONE, DRONE, OVERLORD]

    def build_order_two_base_roach_01(self):
        self.bot.build_order_label = "two base roach 01"
        self.bot.build_order_list = [DRONE, OVERLORD, DRONE, DRONE, DRONE,
            SPAWNINGPOOL, DRONE, DRONE, EXTRACTOR, DRONE, DRONE, QUEEN,
            OVERLORD, DRONE, EXTRACTOR, DRONE, DRONE, ROACHWARREN,
            ZERGLING, ZERGLING, DRONE, DRONE, DRONE, DRONE, DRONE, OVERLORD,
            HATCHERY]

    def build_order_two_base_roach_02(self):
        self.bot.build_order_label = "two base roach 02"
        self.bot.build_order_list = [DRONE, OVERLORD, DRONE, DRONE, DRONE,
            HATCHERY, DRONE, SPAWNINGPOOL, DRONE, DRONE, EXTRACTOR, DRONE,
            DRONE, DRONE, QUEEN, DRONE, EXTRACTOR, DRONE, DRONE, OVERLORD,
            ROACHWARREN, ZERGLING, ZERGLING, DRONE, DRONE, DRONE, DRONE,
            DRONE, ROACH, OVERLORD]

    def build_order_zvz_long(self):
        self.bot.build_order_label = "zvz long"
        self.bot.build_order_list = [NOBOOST, DRONE, OVERLORD, DRONE,
            DRONE, DRONE, SPAWNINGPOOL, DRONE, DRONE, HATCHERY,
            DRONE, DRONE, QUEEN, OVERLORD, EXTRACTOR, DRONE, ZERGLING,
            QUEEN, ZERGLING, DRONE, DRONE, DRONE, DRONE, DRONE, DRONE,
            DRONE, OVERLORD, DRONE, DRONE, DRONE, DRONE, DRONE, DRONE,
            DRONE, DRONE, DRONE, DRONE, EVOLUTIONCHAMBER, LAIR,
            ROACHWARREN]

    def build_order_three_base_roach_01(self):
        self.bot.build_order_label = "three base roach 01"
        self.bot.build_order_list = [DRONE, OVERLORD, DRONE, DRONE, DRONE,
            DRONE, HATCHERY, DRONE, DRONE, DRONE, EXTRACTOR, SPAWNINGPOOL,
            OVERLORD, DRONE, DRONE, DRONE, QUEEN, QUEEN, ZERGLING, ZERGLING,
            ZERGLING, ZERGLING, HATCHERY, DRONE, DRONE, DRONE, DRONE, DRONE,
            DRONE, DRONE, DRONE, OVERLORD, DRONE, DRONE, DRONE, DRONE, DRONE,
            DRONE, OVERLORD, QUEEN, QUEEN, OVERLORD]

    def build_order_1_hydra(self):
        self.bot.build_order_label = "1 base hydra"
        self.bot.build_order_list = [DRONE, DRONE, EXTRACTOR, OVERLORD,
        DRONE, DRONE, DRONE, SPAWNINGPOOL, DRONE, DRONE, DRONE, DRONE, LAIR,
        EXTRACTOR, DRONE, DRONE, OVERLORD, DRONE, DRONE, HYDRALISKDEN]

    def build_order_2_hydra(self):
        self.bot.build_order_label = "2 base hydra"
        self.bot.build_order_list = [DRONE, OVERLORD, DRONE, DRONE, DRONE,
        HATCHERY, DRONE, EXTRACTOR, DRONE, SPAWNINGPOOL, DRONE, DRONE, DRONE,
        DRONE, LAIR, ROACHWARREN, DRONE, DRONE, ZERGLING, ZERGLING,
        OVERLORD, EXTRACTOR, DRONE, DRONE, DRONE, DRONE, HYDRALISKDEN,
        QUEEN, DRONE, QUEEN]

    def build_order_fast_four_hatchery(self):
        self.bot.build_order_label = "fast four hatchery"
        self.bot.build_order_list = [HATCHERY, HATCHERY, HATCHERY]

    def build_order_1_broodlord(self):
        self.bot.build_order_label = "1 broodlord"
        self.bot.build_order_list = [NOBOOST, DRONE, DRONE, EXTRACTOR,
            SPAWNINGPOOL, DRONE, OVERLORD, DRONE, DRONE, DRONE, LAIR,
            EXTRACTOR, DRONE, DRONE, DRONE, DRONE, DRONE,
            INFESTATIONPIT, OVERLORD, QUEEN, DRONE, DRONE, DRONE, DRONE,
            OVERLORD, ZERGLING, ZERGLING, DRONE, HIVE, SPIRE, DRONE]
        self.melee_first_upgrades()

    def build_order_2_broodlord(self):
        self.bot.build_order_label = "2 broodlord"
        self.bot.build_order_list = [NOBOOST, DRONE, OVERLORD, DRONE,
            DRONE, DRONE, HATCHERY, DRONE, DRONE, SPAWNINGPOOL, DRONE,
            DRONE, DRONE, DRONE, EXTRACTOR, DRONE, DRONE, DRONE,
            QUEEN, QUEEN, OVERLORD, DRONE, DRONE, DRONE, DRONE,
            LAIR, ROACHWARREN, DRONE, DRONE, DRONE, DRONE, EXTRACTOR,
            DRONE, INFESTATIONPIT, OVERLORD, DRONE, DRONE,
            ROACH, ROACH, OVERLORD,
            ROACH, ROACH, ROACH, ROACH, OVERLORD,
            ROACH, ROACH, DRONE, DRONE, HIVE, SPIRE, DRONE, DRONE, DRONE,
            OVERLORD]

    def build_order_zvt_standard_1(self):
        self.bot.build_order_label = "zvt std 1"
        self.bot.build_order_list = [NOBOOST, DRONE, OVERLORD, DRONE,
            DRONE, DRONE, HATCHERY, DRONE, DRONE, SPAWNINGPOOL]

    def build_order_zvt_standard_2(self):
        self.bot.build_order_label = "zvt std 2"
        self.bot.build_order_list = [NOBOOST, DRONE, OVERLORD, DRONE,
            DRONE, DRONE, HATCHERY, DRONE, DRONE, SPAWNINGPOOL,
            DRONE, DRONE, DRONE, DRONE]

    def build_order_zvt_anti_p_1(self):
        self.bot.build_order_label = "zvt anti p 1"
        self.bot.build_order_list = [DRONE, EXTRACTOR, SPAWNINGPOOL,
            DRONE, DRONE, OVERLORD, DRONE, BOOST, ZERGLING, ZERGLING, ZERGLING,
            ZERGLING, ZERGLING, ZERGLING, OVERLORD, ZERGLING, ZERGLING,
            ZERGLING, ZERGLING]
        self.melee_first_upgrades()

    def build_order_zvt_anti_a_1(self):
        self.bot.build_order_label = "zvt anti a 1"
        self.bot.build_order_list = [DRONE, DRONE, EXTRACTOR, DRONE,
            SPAWNINGPOOL, DRONE, OVERLORD, DRONE, LAIR, ZERGLING,
            ZERGLING, DRONE, DRONE, DRONE, ROACHWARREN, OVERLORD,
            QUEEN, DRONE, DRONE, NYDUSNETWORK, RESEARCH_BURROW, OVERLORD,
            ROACH, ROACH, QUEEN, NYDUSCANAL, ROACH, ROACH]

    def build_order_queen_21(self):
        self.bot.build_order_label = "queen 21"
        self.bot.build_order_list = [NOBOOST, DRONE, OVERLORD, DRONE, DRONE,
        DRONE, HATCHERY, DRONE, DRONE, DRONE, EXTRACTOR, SPAWNINGPOOL,
        DRONE, DRONE, DRONE, DRONE, DRONE, QUEEN, BOOST, QUEEN, DRONE,
        OVERLORD, ZERGLING, DRONE]

    def build_order_crackling(self):
        self.bot.build_order_label = "crackling"
        self.bot.build_order_list = [NOBOOST, DRONE, OVERLORD, DRONE, DRONE,
        DRONE, HATCHERY, DRONE, DRONE, DRONE, EXTRACTOR, SPAWNINGPOOL,
        DRONE, DRONE, DRONE, DRONE, DRONE, QUEEN, BOOST, QUEEN, DRONE,
        OVERLORD, ZERGLING, DRONE]
        # self.bot.build_upgrade_list = [ZERGMELEEWEAPONSLEVEL1,
        # ZERGMELEEWEAPONSLEVEL2, ZERGGROUNDARMORSLEVEL1,
        # ZERGGROUNDARMORSLEVEL2, ZERGMELEEWEAPONSLEVEL3,
        # ZERGGROUNDARMORSLEVEL3, ZERGMISSILEWEAPONSLEVEL1,
        # ZERGMISSILEWEAPONSLEVEL2, ZERGMISSILEWEAPONSLEVEL3]
        self.melee_first_upgrades()

    def build_order_baneling_bust(self):
        self.bot.build_order_label = "baneling bust"
        self.bot.build_order_list = [NOBOOST, DRONE, OVERLORD, DRONE, DRONE,
        DRONE, HATCHERY, DRONE, DRONE, DRONE, EXTRACTOR, SPAWNINGPOOL,
        DRONE, DRONE, DRONE, DRONE, DRONE, QUEEN, BOOST, QUEEN, BANELINGNEST,
        DRONE, OVERLORD, ZERGLING, DRONE]
        # self.bot.build_upgrade_list = [ZERGMELEEWEAPONSLEVEL1,
        # ZERGMELEEWEAPONSLEVEL2, ZERGGROUNDARMORSLEVEL1,
        # ZERGGROUNDARMORSLEVEL2, ZERGMELEEWEAPONSLEVEL3,
        # ZERGGROUNDARMORSLEVEL3, ZERGMISSILEWEAPONSLEVEL1,
        # ZERGMISSILEWEAPONSLEVEL2, ZERGMISSILEWEAPONSLEVEL3]
        self.melee_first_upgrades()

    def build_order_defend_zergling_rush(self):
        self.bot.build_order_label = "defend zergling rush"
        self.bot.build_order_list = [DRONE, NOEVO, DRONE, SPAWNINGPOOL,
        DRONE, EXTRACTOR, OVERLORD, DRONE, DRONE, DRONE, DRONE, DRONE,
        QUEEN, BOOST, ZERGLING, OVERLORD, ZERGLING, ZERGLING]

    def build_order_defend_proxy_gateway(self):
        self.bot.build_order_label = "defend proxy gateway"
        self.bot.build_order_list = [NOBOOST, DRONE, NOEVO, NOZERGLING, DRONE,
        SPAWNINGPOOL, DRONE, OVERLORD, EXTRACTOR, DRONE, DRONE, DRONE,
        DRONE, DRONE, ROACHWARREN, QUEEN, DRONE, OVERLORD, DRONE]

    def build_order_defend_proxy_gateway_02(self):
        self.bot.build_order_label = "defend proxy gateway 02"
        self.bot.build_order_list = [NOBOOST, DRONE, NOEVO, OVERLORD, DRONE,
        DRONE, DRONE, DRONE, SPAWNINGPOOL, DRONE, EXTRACTOR, DRONE, DRONE, DRONE,
        DRONE, QUEEN, ROACHWARREN, DRONE, OVERLORD, DRONE, DRONE]

    def build_order_defend_proxy_gateway_02(self):
        self.bot.build_order_label = "defend proxy gateway 02"
        self.bot.build_order_list = [NOBOOST, DRONE, NOEVO, OVERLORD, DRONE,
        DRONE, DRONE, DRONE, SPAWNINGPOOL, DRONE, EXTRACTOR, DRONE, DRONE, DRONE,
        DRONE, QUEEN, ROACHWARREN, DRONE, OVERLORD, DRONE, DRONE]

    def build_order_defend_cannon_rush(self):
        self.bot.build_order_label = "defend cannon rush"
        self.bot.build_order_list = [NOBOOST, DRONE, NOEVO, NOZERGLING,
        OVERLORD, DRONE, DRONE, DRONE, DRONE, HATCHERY, DRONE, DRONE, DRONE,
        SPAWNINGPOOL, DRONE, EXTRACTOR, DRONE, DRONE, QUEEN, ROACHWARREN,
        EXTRACTOR, OVERLORD]

    def build_order_defend_cannon_rush_02(self):
        self.bot.build_order_label = "defend cannon rush 02"
        self.bot.build_order_list = [NOBOOST, DRONE, NOEVO, DRONE,
        SPAWNINGPOOL, OVERLORD, DRONE, DRONE, DRONE, DRONE, DRONE,
        EXTRACTOR, DRONE, DRONE, QUEEN, ROACHWARREN,
        EXTRACTOR, OVERLORD]

    def build_order_worker_rush(self):
        self.bot.build_order_label = "worker rush"
        self.bot.build_order_list = [DRONE, DRONE]

    def build_order_test_scout_timing(self):
        self.bot.build_order_label = "test scout timing"
        self.bot.build_order_list = [DRONE, OVERLORD, DRONE, DRONE, DRONE,
            DRONE, DRONE, DRONE, EXTRACTOR, SPAWNINGPOOL, DRONE]

    def build_order_test_cancel_hatch(self):
        self.bot.build_order_label = "test cancel hatch"
        self.bot.build_order_list = [DRONE, OVERLORD, DRONE, DRONE, DRONE,
            HATCHERY]

    def melee_first_upgrades(self):
        self.bot.build_upgrade_list = [ZERGMELEEWEAPONSLEVEL1,
        ZERGMELEEWEAPONSLEVEL2, ZERGGROUNDARMORSLEVEL1,
        ZERGGROUNDARMORSLEVEL2, ZERGMELEEWEAPONSLEVEL3,
        ZERGGROUNDARMORSLEVEL3, ZERGMISSILEWEAPONSLEVEL1,
        ZERGMISSILEWEAPONSLEVEL2, ZERGMISSILEWEAPONSLEVEL3]

    def build_order_upgrades(self):
        self.bot.build_upgrade_list = [ZERGMISSILEWEAPONSLEVEL1,
        ZERGGROUNDARMORSLEVEL1, ZERGMELEEWEAPONSLEVEL1,
        ZERGMISSILEWEAPONSLEVEL2, ZERGGROUNDARMORSLEVEL2,
        ZERGMELEEWEAPONSLEVEL2, ZERGMISSILEWEAPONSLEVEL3,
        ZERGGROUNDARMORSLEVEL3, ZERGMELEEWEAPONSLEVEL3]

    async def read_build_order(self):
        await self.pick_build()
        print(f"Build order: {self.bot.build_order}")

        await self.read_the_order()
        print(f"Build description: {self.bot.build_order_label}")
        self.bot.write_data.print_build_order()

        print("build order list: {}".format(self.bot.build_order_list))
        list(self.bot.build_order_list)

        print("build upgrade list: {}".format(self.bot.build_upgrade_list))
        list(self.bot.build_upgrade_list)

    async def build_details(self):
        """If build never uses hive tech, don't build hive.
        If build never uses infestation pit, don't build it.
        """
        if (self.bot.build_order in [BuildOrderId.SHORT,
            BuildOrderId.ONE_BASE_BROODLORD, BuildOrderId.TWO_BASE_BROODLORD,
            BuildOrderId.CRACKLING, BuildOrderId.BANELING_BUST]
        ):
            self.build_hive = True

        if  (self.bot.build_order in [BuildOrderId.PIG_SWARM,
            BuildOrderId.TL_SWARM] or
            self.build_hive
        ):
            self.build_infestation_pit = True

        if (self.bot.build_order in [BuildOrderId.CRACKLING,
            BuildOrderId.BANELING_BUST]
        ):
            self.build_roach_warren = False
            self.build_hydralisk_den = False

    async def gateway(self):
        if (self.bot.minerals >= 900 or
            self.bot.supply_used > 160
        ):
            self.build_infestation_pit = True
            self.build_hive = True

        if self.bot.structures(HIVE).ready:
            self.build_roach_warren = True
            self.build_hydralisk_den = True

        if self.bot.time > 330:
            self.boost_flag = True
            self.evo_flag = True
            self.zergling_flag = True

    async def pick_build(self):
        # comment out next line for production
        # self.bot.production_version = False
        if not self.bot.production_version:
            self.bot.opponent_name = 'DoogieHowitzer'

        if self.bot.opponent_name == 'Blunty':
            self.build_list = [66, 66, 90, 250, 260]
        elif self.bot.opponent_name == 'dronedronedrone':
            self.build_list = [66, 66, 90, 250, 260]
        elif self.bot.opponent_name == 'LucidZJS':
            self.build_list = [66, 66, 66, 90, 250]
        elif self.bot.opponent_name == 'NikbotZerg':
            self.build_list = [BuildOrderId.HATCH_16]
        elif self.bot.opponent_name == 'QueenBot':
            self.build_list = [250]
        elif self.bot. opponent_name == 'Sproutch':
            self.build_list = [60, 80, 250]
        elif self.bot.opponent_name == 'TyrZ':
            self.build_list = [66, 66, 66, 90, 250]
        elif self.bot.opponent_name == 'WorthlessBot':
            self.build_list = [BuildOrderId.DEFEND_ZERGLING_RUSH]
        elif self.bot.opponent_name == 'Zerg001':
            self.build_list == [BuildOrderId.CRACKLING]
        elif self.bot.opponent_name == 'Zidolina':
            self.build_list = [65]
        elif self.bot.race_enemy == Race.Zerg:
            self.build_list = [66, 66, 90, 250, 260]

        elif (self.bot.opponent_id == 'c5e0e203-bfa8-4f8f-a96d-5235a9a481af' or
            self.bot.opponent_id == '2540c0f3-238f-40a7-9c39-2e4f3dca2e2f' or
            self.bot.opponent_id == '30935ec2-f10b-4a74-a0f7-9bcaf713d46a'
        ):
            self.build_list = [BuildOrderId.DEFEND_PROXY_GATEWAY]
            # self.build_list = [42, 82, 90, 90, 150, 250, 260]
        elif self.bot.opponent_name == 'AdditionalPylons':
            self.build_list = [15]
        elif self.bot.opponent_name == 'DoogieHowitzer':
            self.build_list = [BuildOrderId.DEFEND_PROXY_GATEWAY_02]
        elif self.bot.opponent_id == 'ca07071b-d8d4-4bc0-a1cd-a1ddda197631':
            self.build_list = [1, 5, 10, 10]
        elif self.bot.opponent_id == '2994c8ff-7cae-4498-8390-49cf3d8c1d82':
            self.build_list = [43, 75, 80]
        elif self.bot.opponent_id == 'b4d7dc43-3237-446f-bed1-bceae0868e89':
            self.build_list = [1, 5, 10, 120]
        elif self.bot.opponent_name == 'LamaGate':
            self.build_list = [43, 75, 80]
        elif self.bot.race_enemy == Race.Protoss:
            self.build_list = [42, 82, 90, 90, 150, 250, 260]
        elif self.bot.opponent_id == 'd7bd5012-d526-4b0a-b63a-f8314115f101':
            self.build_list = [75]
        elif self.bot.opponent_id == 'e8972ce3-c84b-4269-9a94-d76fc94aa2bc':
            self.build_list = [1, 5, 10, 10, 120]
        elif self.bot.opponent_id == '219e4b0e-dcce-493b-ae80-302cbcb8af73':
            self.build_list = [49]
        elif self.bot.opponent_id == 'b7c17894-8f38-423b-87d2-f983065364f3':
            self.build_list = [120]
        elif self.bot.opponent_id == 'c25e8cb3-1baf-42d7-b744-4be6ba28a5f5':
            self.build_list = [280]
        elif self.bot.opponent_id == '5714a116-b8c8-42f5-b8dc-93b28f4adf2d':
            self.build_list = [42, 47, 49, 90, 90, 90, 90, 101, 110, 120, 120, 120]
        elif self.bot.opponent_id == 'a7b9a217-919d-46d5-b214-3fea6e92b15c':
            self.build_list = [42, 47, 49, 101, 110, 120, 120, 120, 250, 260]
        elif self.bot.opponent_id == '76cc9871-f9fb-4fc7-9165-d5b748f2734a':
            self.build_list = [1, 5, 10, 10]
        elif self.bot.race_enemy == Race.Terran:
            self.build_list = [42, 47, 49, 90, 90, 101, 110, 120, 120, 120, 250, 260]
        else:
            self.build_list = [42, 47, 49, 65, 66, 90, 250, 260]

        # special magic
        if random.random() <.8:
            self.build_list = dir(BuildOrderId)
        # end special magic

        self.bot.build_order = random.choice(self.build_list)

        if len(self.bot.build_upgrade_list) == 0:
            self.build_order_upgrades()

        await self.build_details()


    async def Run(self):
        if self.bot.iteration == 0:
            if self.bot.production_version:
                self.build_log_level = False
                self.build_log_level_extractor = False
                self.build_log_level_hatch = False
                self.build_log_level_ultra = False

            await self.read_build_order()

        self.hatch_count = (self.bot.hatcheries.amount +
                self.bot.structures(LAIR).amount + self.bot.structures(HIVE).amount)

        if self.bot.roach_rush_flag:
            await self.roach_rush_defense()
            return
        else:
            await self.gateway()

        if self.bot.build_defense_flag:
            await self.rush_defense_strategy()
        elif len(self.bot.build_order_list) > 0:
            await self.build_order()
        else:
            await self.strategy()

    async def build_order(self):
        await self.supply_check()
        await self.extractor_pending_check()
        if len(self.bot.build_order_list) > 0:
            build_next = self.bot.build_order_list[0]
            #print("build next: " + str(build_next))
        else:
            return

        if build_next == HATCHERY:
            await self.expansion_check()
        elif build_next == DRONE:
            await self.drone_check()
        elif build_next == OVERLORD:
            await self.supply_check()
        elif build_next == MORPH_OVERLORDTRANSPORT:
            await self.dropper_lord_check()
        elif build_next == SPAWNINGPOOL:
            await self.spawning_pool_check()
        elif build_next == NOBOOST:
            await self.no_boost()
        elif build_next == SPINECRAWLER:
            await self.spine_crawler_check()
        elif build_next == SPORECRAWLER:
            await self.spore_crawler_check()
        elif build_next == BOOST:
            await self.boost()
        elif build_next == EXTRACTOR:
            await self.extractor_check()
        elif build_next == NOZERGLING:
            await self.no_zergling()
        elif build_next == ZERGLING:
            await self.zergling_check()
        elif build_next == BANELING:
            await self.baneling_check()
        elif build_next == QUEEN:
            await self.queen_check()
        elif build_next == BANELINGNEST:
            await self.baneling_nest_check()
        elif build_next == ROACHWARREN:
            await self.roach_warren_check()
        elif build_next == ROACH:
            await self.roach_check()
        elif build_next == CORRUPTOR:
            await self.corruptor_check()
        elif build_next == BROODLORD:
            await self.broodlord_check()
        elif build_next == LAIR:
            await self.lair_check()
        elif build_next == EVOLUTIONCHAMBER:
            await self.evo_chamber_check()
        elif build_next == NOEVO:
            await self.no_evo()
        elif build_next == EVO:
            await self.evo()
        elif build_next == RESEARCH_BURROW:
            await self.research_burrow_check()
        elif build_next == INFESTATIONPIT:
            await self.inf_pit_check()
        elif build_next == HYDRALISKDEN:
            await self.hydra_den_check()
        elif build_next == LURKERDENMP:
            await self.lurker_den_check()
        elif build_next == SPIRE:
            await self.spire_check()
        elif build_next == SWARMHOSTMP:
            await self.swarm_host_check()
        elif build_next == NYDUSNETWORK:
            await self.nydus_network_check()
        elif build_next == NYDUSCANAL:
            await self.nydus_canal_check()
        elif build_next == HIVE:
            await self.hive_check()
        elif build_next == GREATERSPIRE:
            await self.greater_spire_check()

    async def strategy(self):
        await self.economy()
        if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == HATCHERY
            and self.bot.minerals < 350
        ):
            if self.hatchery_bounces < 10:
                print("hatchery bounce")
                self.hatchery_bounces += 1
            return

        if (self.bot.build_order == BuildOrderId.PIG_SWARM or
            self.bot.build_order == BuildOrderId.TL_SWARM):
            await self.swarm_host_check()
        await self.adrenal_glands_check()
        await self.metabolic_boost_check()
        await self.baneling_speed_check()
        await self.broodlord_check()
        await self.corruptor_check()
        await self.greater_spire_check()
        await self.spire_check()
        await self.dropper_lord_check()
        await self.overseer_check()
        await self.evolution_upgrade_check()
        await self.hydra_upgrade_check()
        await self.hydra_check()
        # await self.lurker_check()
        await self.roach_check()
        await self.baneling_check()
        await self.zergling_check()
        await self.spawning_pool_check()
        await self.queen_check()
        # await self.baneling_nest_check()
        await self.lair_check()
        await self.roach_warren_check()
        await self.evo_chamber_check()
        await self.research_burrow_check()
        await self.roach_speed_check()
        await self.hydra_den_check()
        # await self.lurker_den_check()
        await self.spine_crawler_check()
        await self.spore_crawler_check()
        await self.inf_pit_check()
        await self.hive_check()

        await self.nydus_network_check()
        await self.nydus_canal_check()

    async def rush_defense_strategy(self):
        if len(self.bot.build_order_list) > 0:
            self.bot.build_order_list = []
        if self.bot.zergling_rush:
            await self.zergling_rush_defense()
        else:
            await self.rush_defense_build()
        if self.bot.time > 300:
            await self.resume_normal_strategy()

    async def resume_normal_strategy(self):
        self.bot.build_defense_flag = False
        self.bot.zergling_rush = False
        self.bot.roach_rush_flag = False
        self.bot.proxy_pylon = False
        self.bot.proxy_rush_flag = False

    async def zergling_rush_defense(self):
        await self.spawning_pool_check()
        await self.supply_check()
        await self.queen_check()
        await self.metabolic_boost_check()
        await self.spine_crawler_check()
        await self.zergling_check()

    async def roach_rush_defense(self):
        await self.rush_defense_build()
        if self.bot.time > 840:
            await self.resume_normal_strategy()

    async def rush_defense_build(self):
        self.build_roach_warren = True
        await self.spawning_pool_check()
        await self.roach_warren_check()
        await self.spine_crawler_check()
        await self.supply_check()
        await self.queen_check()
        await self.roach_check()
        await self.ideal_harvester_count()
        await self.assigned_harvester_count()
        await self.drone_check()
        await self.extractor_check()

    async def economy(self):
        await self.ideal_harvester_count()
        await self.assigned_harvester_count()
        await self.expansion_check()
        if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == HATCHERY
            and self.bot.minerals < 350
        ):
            return
        await self.supply_check()
        await self.drone_check()
        await self.extractor_check()

    async def ideal_harvester_count(self):
        self.ideal_mineral_harvesters = 0
        for th in self.bot.zerg_towns:
            self.ideal_mineral_harvesters +=th.ideal_harvesters

        self.ideal_vespene_harvesters = 0
        for ex in self.bot.structures(EXTRACTOR):
            self.ideal_vespene_harvesters +=ex.ideal_harvesters

    async def assigned_harvester_count(self):
        self.assigned_mineral_harvesters = 0
        for th in self.bot.zerg_towns:
            self.assigned_mineral_harvesters +=th.assigned_harvesters

        self.assigned_vespene_harvesters = 0
        for ex in self.bot.structures(EXTRACTOR):
            self.assigned_vespene_harvesters +=ex.assigned_harvesters

    async def supply_check(self):
        if (self.bot.minerals >= 100 and len(self.bot.units(LARVA)) > 0):
            await self.max_supply_check()
            if self.supply_cap_reached:
                return
            if len(self.bot.build_order_list) > 0:
                await self.build_order_overlord_check()
            else:
                await self.overlord_check()

    async def max_supply_check(self):
        h = len(self.bot.zerg_towns_ready)
        ov = len(self.bot.units.filter(lambda o:
            o.type_id in [UnitTypeId.OVERLORD, UnitTypeId.OVERLORDCOCOON,
            UnitTypeId.OVERSEER, UnitTypeId.TRANSPORTOVERLORDCOCOON,
            UnitTypeId.OVERLORDTRANSPORT, UnitTypeId.OVERSEERSIEGEMODE]))
        p = self.bot.already_pending(OVERLORD)
        supply_cap = h * 6 + ov * 8
        if supply_cap >= 208:
            self.supply_cap_reached = True
        else:
            self.supply_cap_reached = False

    async def build_order_overlord_check(self):
        if self.bot.build_order_list[0] == OVERLORD:
            await self.build_overlord()
            self.bot.build_order_list.pop(0)
            # await self.print_build_order_list()
        elif self.bot.time < 60:
            pass
        else:
            if self.bot.supply_left + (8 * self.bot.already_pending(OVERLORD)) < 2:
                await self.build_overlord()

    async def overlord_check(self):
        supply_left = (self.bot.supply_left +
                      (8 * self.bot.already_pending(OVERLORD)))
        h = len(self.bot.zerg_towns_ready)
        l = len(self.bot.units(LARVA))
        q = min(len(self.bot.units(QUEEN)), len(self.bot.zerg_towns_ready))
        p = self.bot.already_pending(OVERLORD)
        if (self.bot.structures(ROACHWARREN).ready.exists
        or  self.bot.structures(HYDRALISKDEN).ready.exists):
            c = 2
        else:
            c = 1
        if supply_left < (l + q * 3 - p * 8 + h + 1) * c:
            await self.build_overlord()

    async def build_overlord(self):
        self.bot.units(LARVA).random.train(OVERLORD)
        if self.build_log_level:
            print('start overlord')
        # self.bot.write_data.start_overlord()

    async def dropper_lord_check(self):
        build_dropper_lord_flag = False
        if (len(self.bot.units(OVERLORD)) > 0
        and self.bot.can_afford(MORPH_OVERLORDTRANSPORT)):
            if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == MORPH_OVERLORDTRANSPORT):
                await self.build_dropper_lord()

    async def build_dropper_lord(self):
        o = self.bot.units(OVERLORD).closest_to(self.bot.away)
        if self.build_log_level:
            print('overlord:', o)
            print('can afford:', self.bot.can_afford(MORPH_OVERLORDTRANSPORT))
        o(AbilityId.MORPH_OVERLORDTRANSPORT)
        if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == MORPH_OVERLORDTRANSPORT
        ):
                self.bot.build_order_list.pop(0)
                # await self.print_build_order_list()
                # self.bot.write_data.start_dropper_lord()

    async def overseer_check(self):
        lairs = len(self.bot.structures.filter(lambda b:
                b.type_id in [UnitTypeId.LAIR] or
                b.type_id in [UnitTypeId.HIVE]))
        if (lairs > 0 and len(self.bot.units(OVERLORD)) > 0
            and self.bot.can_afford(MORPH_OVERSEER)
        ):
            if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == MORPH_OVERSEER):
                self.pos = self.bot.away
                await self.build_overseer()
            else:
                await self.strat_overseer()

    async def strat_overseer(self):
        if self.bot.time > 480:
            return

        overseers = len(self.bot.units(OVERSEER)) + self.bot.already_pending(OVERSEER)
        if overseers == 0:
            self.pos = self.bot.rushwait_location
        elif overseers == 1:
            self.pos = self.bot.home
        else:
            return
        await self.build_overseer()

    async def build_overseer(self):
        o = self.bot.units(OVERLORD).closest_to(self.pos)
        print('start overseer')
        o(AbilityId.MORPH_OVERSEER)
        if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == MORPH_OVERSEER
        ):
            self.bot.build_order_list.pop(0)
            # await self.print_build_order_list()
            # self.bot.write_data.start_overseer()
        if o.position.distance_to(self.pos) > 2:
            o.move(self.pos)

    async def drone_check(self):
        if (self.bot.supply_left > 0 and len(self.bot.units(LARVA)) > 0
        and self.bot.minerals >= 50):
            if self.bot.build_defense_flag:
                await self.defense_drone_check()
            if (len(self.bot.build_order_list)) > 0:
                await self.build_order_drone_check()
            else:
                await self.train_drone_check()

    async def build_order_drone_check(self):
        if self.bot.build_order_list[0] == DRONE:
            await self.train_drone()
            self.bot.build_order_list.pop(0)

    async def defense_drone_check(self):
        if (self.bot.proxy_pylon or
            self.bot.proxy_rush_flag
        ):
            if not self.bot.structures(ROACHWARREN).exists:
                await self.train_drone_check()
        elif (not self.bot.structures(SPAWNINGPOOL).ready.exists and
            not self.bot.structures(ROACHWARREN).exists
        ):
            await self.train_drone_check()

    async def train_drone_check(self):
        d = len(self.bot.units(DRONE)) + self.bot.already_pending(DRONE)
        id_har = (self.ideal_mineral_harvesters +
                  self.ideal_vespene_harvesters)

        if d > id_har + 2:
            pass
        elif d < 35:
            await self.train_drone()
        elif d > 84:
            pass
        elif d > 76 and self.bot.race_enemy == Race.Zerg:
            pass
        else:
            await self.train_drone()


    async def train_drone(self):
        self.bot.units(LARVA).random(AbilityId.LARVATRAIN_DRONE)
        if self.build_log_level:
            print('train drone')
        # if len(self.bot.build_order_list) > 0:
            # if self.bot.build_order_list[0] == DRONE:
                # self.bot.build_order_list.pop(0)
                # await self.print_build_order_list()
        # self.bot.write_data.train_drone()

    async def spine_crawler_check(self):
        if (self.bot.structures(SPAWNINGPOOL).ready.exists
        and self.bot.can_afford(SPINECRAWLER)
        and len(self.bot.units(DRONE)) > 0):
            await self.spine_crawler_check_2()

    async def spine_crawler_check_2(self):
        if (self.bot.zergling_rush or
            self.bot.roach_rush_flag or
            self.bot.proxy_rush_flag
        ):
            await self.rush_spine_crawler()
        elif (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == SPINECRAWLER):
                await self.pick_spine_crawler_location()

    async def pick_spine_crawler_location(self):
        for h in (self.bot.hatcheries |
                self.bot.structures(LAIR) | self.bot.structures(HIVE)):
            for d in range(4, 15):
                self.target = h.position.to2.towards(self.bot.game_info.map_center, d)
                if await self.bot.can_place(SPINECRAWLER, self.target):
                    self.d = self.bot.workers.closest_to(self.target)
                    await self.start_spine_crawler()
                    return

    async def rush_spine_crawler(self):
        cs = ((self.bot.structures(SPINECRAWLER)).amount +
            self.bot.already_pending(SPINECRAWLER))
        if ((self.bot.proxy_rush_flag and cs  < 3 and
            self.bot.opponent_id == '2540c0f3-238f-40a7-9c39-2e4f3dca2e2f') or
            ((self.bot.zergling_rush or self.bot.proxy_rush_flag) and cs  < 1) or
            (self.bot.roach_rush_flag and cs  < 3)
        ):
            await self.rush_pick_spine_location()

    async def rush_pick_spine_location(self):
        self.near = self.bot.ramp_location
        await self.pick_build_location_1()

        for self.r in range(0, -8, -1):
            await self.pick_build_location_2()
            if await self.bot.can_place(SPINECRAWLER, self.target):
                await self.pick_drone_to_build()
                await self.start_spine_crawler()
                return

    async def start_spine_crawler(self):
        # if len(self.bot.units(DRONE)) > 0:
        if self.d:
            self.d.build(SPINECRAWLER, self.target)
            if self.build_log_level:
                print('start spine crawler')
            # self.bot.write_data.start_spine_crawler()

            if (len(self.bot.build_order_list) > 0
                and self.bot.build_order_list[0] == SPINECRAWLER
            ):
                self.bot.build_order_list.pop(0)

    async def spore_crawler_check(self):
        build_spore_crawler_flag = False
        if (self.bot.structures(SPAWNINGPOOL).ready.exists
        and self.bot.can_afford(SPORECRAWLER)
        and len(self.bot.units(DRONE)) > 0):
            if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == SPORECRAWLER):
                build_spore_crawler_flag = True

        if build_spore_crawler_flag:
            await self.pick_spore_crawler_location()

    async def pick_spore_crawler_location(self):
        for h in (self.bot.hatcheries |
            self.bot.structures(LAIR) |
            self.bot.structures(HIVE)):
            for d in range(4, 15):
                self.pos = h.position.to2.towards(self.bot.game_info.map_center, d)
                if await self.bot.can_place(SPORECRAWLER, self.pos):
                    await self.start_spore_crawler()
                    return

    async def start_spore_crawler(self):
        if len(self.bot.units(DRONE)) > 0:
            drone = self.bot.workers.closest_to(self.pos)
            drone.build(SPORECRAWLER, self.pos)
            if self.build_log_level:
                print('start spore crawler')
            # self.bot.write_data.start_spore_crawler()

            if (len(self.bot.build_order_list) > 0
                and self.bot.build_order_list[0] == SPORECRAWLER
            ):
                self.bot.build_order_list.pop(0)
                # await self.print_build_order_list()

    async def no_zergling(self):
        self.zergling_flag = False
        if (len(self.bot.build_order_list) > 0 and
            self.bot.build_order_list[0] == NOZERGLING
        ):
            self.bot.build_order_list.pop(0)

    async def zergling_check(self):
        if (self.bot.structures(SPAWNINGPOOL).ready.exists
            and len(self.bot.units(LARVA)) > 0
            and self.bot.supply_left > 0
            and self.bot.can_afford(ZERGLING)
        ):
            if (len(self.bot.build_order_list) > 0
                and self.bot.build_order_list[0] == ZERGLING
            ):
                await self.train_zerglings()
            else:
                await self.strat_zerglings()

    async def strat_zerglings(self):
        if ((self.bot.structures(ROACHWARREN).ready.exists and
            self.bot.vespene >= 25) or
            not self.zergling_flag
        ):
            return
        else:
            await self.train_zerglings()

    async def train_zerglings(self):
        self.bot.units(LARVA).random.train(ZERGLING)
        if self.build_log_level:
            print('start zerglings')
        if len(self.bot.build_order_list) > 0:
            if self.bot.build_order_list[0] == ZERGLING:
                self.bot.build_order_list.pop(0)
                # await self.print_build_order_list()
        # self.bot.write_data.start_zerglings()

    async def baneling_check(self):
        if (self.bot.structures(BANELINGNEST).ready and
            (self.bot.supply_left > 0) and
            self.bot.can_afford(BANELING)
        ):
            if (len(self.bot.build_order_list) > 0 and
                self.bot.build_order_list[0] == BANELING
            ):
                await self.train_banelings()
            else:
                await self.strat_banelings()

    async def strat_banelings(self):
        if len(self.bot.units(ZERGLING)) > len(self.bot.units(BANELING)):
            await self.train_banelings()

    async def train_banelings(self):
        self.bot.units(ZERGLING).random.train(BANELING)
        if self.build_log_level:
            print('start banelings')
        if len(self.bot.build_order_list) > 0:
            if self.bot.build_order_list[0] == BANELING:
                self.bot.build_order_list.pop(0)
                # await self.print_build_order_list()
        # self.bot.write_data.start_banelings()

    async def baneling_speed_check(self):
        if ((self.bot.structures(LAIR).ready or
            self.bot.structures(HIVE).ready) and
            self.bot.structures(BANELINGNEST).ready and
            self.bot.can_afford(RESEARCH_CENTRIFUGALHOOKS) and not
            self.bot.baneling_speed_started
        ):
            await self.start_banelinig_speed()

    async def start_banelinig_speed(self):
        bn = self.bot.structures(BANELINGNEST).ready
        started = bn.first(RESEARCH_CENTRIFUGALHOOKS)
        if not started:
            return

        self.bot.baneling_speed_started = True
        print('start centfigual hooks')
        if len(self.bot.build_order_list) > 0:
            if self.bot.build_order_list[0] == RESEARCH_CENTRIFUGALHOOKS:
                self.bot.build_order_list.pop(0)
        # self.bot.write_data.print_hooks()

    async def roach_check(self):
        if (self.bot.structures(ROACHWARREN).ready and
            (self.bot.supply_left > 1) and
            self.bot.can_afford(ROACH) and
            len(self.bot.units(LARVA)) > 0
        ):
            if (len(self.bot.build_order_list) > 0 and
                self.bot.build_order_list[0] == ROACH
            ):
                await self.train_roach()
            else:
                await self.strat_roach()

    async def strat_roach(self):
        r = len(self.bot.units(ROACH)) + self.bot.already_pending(ROACH)
        if (self.bot.race_enemy == Race.Zerg or
            self.bot.build_defense_flag or
            r < 8 or
            (r >= 8 and len(self.bot.units(DRONE)) >= 55)
        ):
            await self.train_roach()

    async def train_roach(self):
        self.bot.units(LARVA).random.train(ROACH)
        if self.build_log_level:
            print('start roach')
        if len(self.bot.build_order_list) > 0:
            if self.bot.build_order_list[0] == ROACH:
                self.bot.build_order_list.pop(0)
                # await self.print_build_order_list()
        # self.bot.write_data.start_roach()

    async def hydra_check(self):
        if self.bot.structures(HYDRALISKDEN).ready:
            if ((self.bot.supply_left > 1) and self.bot.can_afford(HYDRALISK)):
                if len(self.bot.units(LARVA)) > 0:
                    await self.train_hydralisk()

    async def train_hydralisk(self):
        self.bot.units(LARVA).random.train(HYDRALISK)
        if self.build_log_level:
            print('start hydralisk')
        if len(self.bot.build_order_list) > 0:
            if self.bot.build_order_list[0] == HYDRALISK:
                self.bot.build_order_list.pop(0)
                # await self.print_build_order_list()
        # self.bot.write_data.start_hydralisk()

    async def lurker_check(self):
        if (self.bot.structures(LURKERDENMP).ready and
            (self.bot.supply_left >= 1) and
            self.bot.can_afford(MORPH_LURKER) and
            len(self.bot.units(HYDRALISK)) > 0
        ):
            if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == MORPH_LURKER):
                await self.build_lurker()
            else:
                await self.strat_lurker()

    async def strat_lurker(self):
        hydras = len(self.bot.units(HYDRALISK))
        lurkers = len(self.bot.units(LURKERMP)) + self.bot.already_pending(LURKERMP)
        if hydras > 3 and lurkers / hydras <= .25:
            await self.build_lurker()

    async def build_lurker(self):
        h = self.bot.units(HYDRALISK).random
        h(AbilityId.MORPH_LURKER)
        print('start lurker')
        if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == MORPH_LURKER
        ):
            self.bot.build_order_list.pop(0)
            # await self.print_build_order_list()
            # self.bot.write_data.start_lurker()

    async def corruptor_check(self):
        if ((self.bot.structures(SPIRE).ready or
            self.bot.structures(GREATERSPIRE).ready) and
            len(self.bot.units(LARVA)) > 0 and
            self.bot.can_afford(CORRUPTOR) and
            self.bot.supply_left > 1
        ):
            if (len(self.bot.build_order_list) > 0 and
                self.bot.build_order_list[0] == CORRUPTOR
            ):
                await self.train_corruptor()
            else:
                await self.strat_corruptor()

    async def strat_corruptor(self):
        if (self.bot.units(CORRUPTOR).amount <
            self.bot.units(BROODLORD).amount + 5
        ):
            await self.train_corruptor()

    async def train_corruptor(self):
        self.bot.units(LARVA).random.train(CORRUPTOR)
        if self.build_log_level:
            print('start corruptor')
        if len(self.bot.build_order_list) > 0:
            if self.bot.build_order_list[0] == CORRUPTOR:
                self.bot.build_order_list.pop(0)
                # await self.print_build_order_list()
        # self.bot.write_data.start_corruptor()

    async def broodlord_check(self):
        if (self.bot.structures(GREATERSPIRE).ready and
            self.bot.can_afford(BROODLORD) and
            self.bot.supply_left > 1
        ):
            if (len(self.bot.build_order_list) > 0 and
                self.bot.build_order_list[0] == BROODLORD
            ):
                await self.train_broodlord()
            else:
                await self.strat_broodlord()

    async def strat_broodlord(self):
        if (self.bot.units(CORRUPTOR).amount >
            self.bot.units(BROODLORD).amount
        ):
            await self.train_broodlord()

    async def train_broodlord(self):
        self.bot.units(CORRUPTOR).random.train(BROODLORD)
        if self.build_log_level:
            print('start broodlord')
        if len(self.bot.build_order_list) > 0:
            if self.bot.build_order_list[0] == BROODLORD:
                self.bot.build_order_list.pop(0)
                # await self.print_build_order_list()
        # self.bot.write_data.start_broodlord()

    async def swarm_host_check(self):
        if self.bot.structures(INFESTATIONPIT).ready.exists:
            if ((self.bot.supply_left >= 3) and self.bot.can_afford(SWARMHOSTMP)):
                if len(self.bot.units(LARVA)) > 0:
                    await self.train_swarm_host()

    async def train_swarm_host(self):
        self.bot.units(LARVA).random.train(SWARMHOSTMP)
        if self.build_log_level:
            print('start swarm host')
        if len(self.bot.build_order_list) > 0:
            if self.bot.build_order_list[0] == SWARMHOSTMP:
                self.bot.build_order_list.pop(0)
                # await self.print_build_order_list()
        # self.bot.write_data.start_swarm_host()

    async def queen_check(self):
        if (self.bot.can_afford(QUEEN) and self.hatch_count > 0
        and self.bot.structures(SPAWNINGPOOL).ready.exists):
            if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == QUEEN):
                self.build_order_queen()
            else:
                self.strat_build_queen()

    def strat_build_queen(self):
        q = len(self.bot.units(QUEEN)) + self.bot.already_pending(QUEEN)
        if self.bot.build_defense_flag and q < 4:
            self.build_queen()
        elif q < 3 and q < self.hatch_count:
            self.build_queen()

    def build_order_queen(self):
        if (self.bot.build_order == BuildOrderId.ZVT_ANTI_A_1 or
            self.bot.build_order == BuildOrderId.PIG_SWARM or
            self.bot.build_order == BuildOrderId.ONE_BASE_FAST_ROACH_02
        ):
            self.pick_location_to_build()
        else:
            self.build_queen()

    def build_queen(self):
        # h_count - idle zerg towns with queen nearby
        # q_count - number of queens not near this idle zerg town
        h_count = 0
        for self.h in self.bot.zerg_towns_ready:
            if self.h.is_idle:
                q_count = 0
                for queen in self.bot.units(QUEEN):
                    if queen.position.distance_to(self.h) < 8:
                        h_count += 1
                        break
                    else:
                        q_count += 1
                if q_count == len(self.bot.units(QUEEN)):
                    self.start_queen()
                    return
        if h_count == 0:
            return
        elif h_count > 1:
            self.pick_location_to_build()

    def pick_location_to_build(self):
        for self.h in self.bot.zerg_towns_ready:
            if self.h.is_idle:
                self.start_queen()
                return

    def start_queen(self):
        self.h.train(QUEEN)
        self.bot.queen_started = True
        if self.build_log_level:
            print('start queen')
        if len(self.bot.build_order_list) > 0:
            if self.bot.build_order_list[0] == QUEEN:
                self.bot.build_order_list.pop(0)
                # await self.print_build_order_list()
        # self.bot.write_data.start_queen()

    async def extractor_pending_check(self):
        if ((self.bot.extractors >
            self.bot.structures(EXTRACTOR).amount +
            self.bot.already_pending(EXTRACTOR))
            and (self.bot.extractor_time + 3 < self.bot.time)
        ):
            self.bot.build_order_list.insert(0, EXTRACTOR)
            self.bot.extractor_time = 999999
            self.bot.extractors = (
                self.bot.structures(EXTRACTOR).amount +
                self.bot.already_pending(EXTRACTOR)
            )

    async def extractor_check(self):
        if (self.bot.can_afford(EXTRACTOR) and
            self.hatch_count > 0 and
            len(self.bot.units(DRONE)) > 0
        ):
            if (len(self.bot.build_order_list) > 0 and
                self.bot.build_order_list[0] == EXTRACTOR
            ):
                await self.pick_extractor_location()
            else:
                await self.strat_extractor_build()

    async def strat_extractor_build(self):
        if (self.bot.vespene > 300 and self.bot.vespene > self.bot.minerals):
            return
        elif self.assigned_vespene_harvesters < self.ideal_vespene_harvesters:
            return

        for h in self.bot.zerg_towns_ready:
            ex = self.bot.structures.filter(lambda b:
                b.type_id == EXTRACTOR and
                b.distance_to(h.position) < 10)
            if len(ex) < 2:
                await self.strat_add_extractor()
                return

    async def strat_add_extractor(self):
        d = len(self.bot.units(DRONE))
        ex = (len(self.bot.structures(EXTRACTOR)) +
            self.bot.already_pending(EXTRACTOR))

        if self.bot.extractors > ex or self.bot.already_pending(EXTRACTOR) > 0:
            return

        h = self.hatch_count
        id_ves = self.ideal_vespene_harvesters

        if h == 1 and ex == 0 and d >= 20:
            await self.pick_extractor_location()
        elif h > 1 and self.bot.vespene < .25 * self.bot.minerals:
            await self.pick_extractor_location()

    async def pick_extractor_location(self):
        for h in (self.bot.hatcheries |
                  self.bot.structures(LAIR) |
                  self.bot.structures(HIVE)):
            vespenes = self.bot.vespene_geyser.closer_than(14.0, h.position)

            for self.vespene in vespenes:
                if not self.bot.structures(EXTRACTOR).closer_than(1.0, self.vespene).exists:
                    self.start_extractor()
                    return

    def start_extractor(self):
        if len(self.bot.units(DRONE)) > 0:
            drone = self.bot.workers.closest_to(self.vespene)
            if self.build_log_level_extractor:
                print(f"Extractor position: {self.vespene.position}")
            drone.build(EXTRACTOR, self.vespene)
            self.extractor_started = True
            if self.build_log_level:
                print('start extractor')
            self.bot.extractors += 1
            if len(self.bot.build_order_list) > 0:
                if self.bot.build_order_list[0] == EXTRACTOR:
                    self.bot.build_order_list.pop(0)
                    self.bot.extractor_time = self.bot.time
                    if self.build_log_level_extractor:
                        print("build order list: {}".format(self.bot.build_order_list))

                    # self.print_build_order_list()
            # self.bot.write_data.start_extractor()

    async def spawning_pool_check(self):
        if (self.bot.structures(SPAWNINGPOOL).exists or
            self.bot.already_pending(SPAWNINGPOOL)
        ):
            return

        if (self.bot.minerals >= 200 and len(self.bot.workers) > 0
        and self.bot.hatchery):
            for d in range(4, 15):
                self.pos = self.bot.hatchery.position.to2.towards(self.bot.game_info.map_center, d)
                if await self.bot.can_place(SPAWNINGPOOL, self.pos):
                    await self.start_spawning_pool()
                    return

    async def start_spawning_pool(self):
        drone = await self.pick_drone_for_pool()
        # drone = self.bot.workers.closest_to(self.pos)
        if drone:
            drone.build(SPAWNINGPOOL, self.pos)
        else:
            return

        self.spawning_pool_started = True
        # self.bot.write_data.start_spawning_pool()
        if self.build_log_level:
            print('start spawning pool')
        if len(self.bot.build_order_list) > 0:
            if self.bot.build_order_list[0] == SPAWNINGPOOL:
                self.bot.build_order_list.pop(0)
                # await self.print_build_order_list()

    async def pick_drone_for_pool(self):
        distance = 20
        drones = self.bot.units.filter(lambda d:
            d.type_id == DRONE
            and d.distance_to(self.pos) < distance
            # and d.tag in self.bot.mineral_drone_tags
            and not d.is_carrying_resource)

        if drones:
            self.d = drones.closest_to(self.pos)
            await self.update_building_drone_tags()
        else:
            self.d = None

        return self.d

    async def update_building_drone_tags(self):
        d = self.d
        self.bot.building_drone_tags.add(d.tag)
        if d in self.bot.mineral_drone_tags:
            self.bot.mineral_drone_tags.remove(d.tag)
        elif d in self.bot.vespene_drone_tags:
            self.bot.vespene_drone_tags.remove(d.tag)

    async def no_boost(self):
        self.boost_flag = False
        if len(self.bot.build_order_list) > 0:
            if self.bot.build_order_list[0] == NOBOOST:
                self.bot.build_order_list.pop(0)

    async def boost(self):
        self.boost_flag = True
        await self.metabolic_boost_check()

    async def metabolic_boost_check(self):
        if (not self.boost_flag) or self.bot.mboost_started:
            return

        metabolic_boost_flag = False

        if (self.bot.minerals >= 100 and self.bot.vespene >= 100
        and self.bot.structures(SPAWNINGPOOL).ready):
            metabolic_boost_flag = True

        if metabolic_boost_flag:
            await self.start_metabolic_boost()
            # await self.move_workers_from_first_gas_check()

    async def start_metabolic_boost(self):
        sp = self.bot.structures(SPAWNINGPOOL).ready
        sp.first(RESEARCH_ZERGLINGMETABOLICBOOST)
        self.bot.mboost_started = True
        print('start metabolic boost')
        if len(self.bot.build_order_list) > 0:
            if self.bot.build_order_list[0] == BOOST:
                self.bot.build_order_list.pop(0)
        # self.bot.write_data.print_boost()

    async def adrenal_glands_check(self):
        if (self.bot.structures(HIVE).ready and
            self.bot.minerals >= 200 and
            self.bot.structures(SPAWNINGPOOL).ready and
            not self.bot.adrenal_glands_started
        ):
            await self.start_adrenal_glands()

    async def start_adrenal_glands(self):
        sp = self.bot.structures(SPAWNINGPOOL).ready
        sp.first(RESEARCH_ZERGLINGADRENALGLANDS)
        self.bot.adrenal_glands_started = True
        print('start adrenal glands')
        if len(self.bot.build_order_list) > 0:
            if self.bot.build_order_list[0] == ADRENAL_GLANDS:
                self.bot.build_order_list.pop(0)
        # self.bot.write_data.print_adrenal_glands()

    async def move_workers_from_first_gas_check(self):
        print("move workers from gas build order")
        if (self.bot.build_order == BuildOrderId.PIG_ELEVATOR or
            self.bot.build_order == BuildOrderId.PIG_SWARM
        ):
            pass
        else:
            print("move workers from gas going to resources")
            await self.bot.resources.move_workers_from_first_gas()

    async def macro_hatchery_check(self):
        # not used - still looking for reasons to build macro hatchery
        if False:
            await self.macro_hatchery_check_2()

    async def macro_hatchery_check_2(self):
        self.near = bot.hatchery.position
        await self.pick_build_location_1()

        for self.r in range(4, 15):
            await self.pick_build_location_2()

            self.bot.worker_for_hatchery = self.bot.workers.closest_to(self.target)
            if await self.bot.can_place(HATCHERY, self.target):
                self.bot.worker_moving_to_hatchery = True
                await self.start_hatchery()
                return

    async def pick_build_location_1(self):
        self.x_sign = math.copysign(1, self.bot.away.x - self.bot.home.x)
        self.y_sign = math.copysign(1, self.bot.away.y - self.bot.home.y)
        if (self.bot.map_name == 'Deathaura LE' or
            self.bot.map_name == 'Disco Bloodbath LE'
        ):
            self.y_sign *= -1

    async def pick_build_location_2(self):
        posx = self.near.x + self.x_sign * self.r
        posy = (self.near.y + self.y_sign *
                random.random() * math.sqrt(225 - self.r ** 2))
        self.target = Point2((posx, posy))

    async def expansion_check(self):
        if len(self.bot.units(DRONE)) > 0:
            if (len(self.bot.build_order_list)) > 0:
                # test 0.4.1.4
                if (self.build_log_level_ultra
                    and self.bot.time > 400
                ):
                    print("late build order hatch check")
                await self.build_order_hatchery_check()
            else:
                await self.strat_hatchery_check()

    async def build_order_hatchery_check(self):
        if self.bot.roach_rush_flag and self.bot.time < 330:
            return

        if self.bot.minerals >= 200:
            await self.moving_to_hatchery_check()

    async def strat_hatchery_check(self):
        if self.bot.roach_rush_flag and self.bot.time < 330:
            return

        if (self.bot.worker_moving_to_hatchery or
            self.bot.already_pending(HATCHERY)
        ):
            return

        if self.bot.minerals > 450:
            await self.need_hatchery_check()
        elif (self.bot.minerals >= 300
            and len(self.bot.zerg_towns) == 1
        ):
            if self.bot.enemy_expansion or self.bot.time >= 150:
                await self.need_hatchery_check()
        elif (len(self.bot.zerg_towns) == 1
            and self.bot.enemy_expansion
        ):
            await self.insert_hatchery()
        else:  # hatchery running out check
            ideal_harvesters = (
                self.ideal_mineral_harvesters +
                self.ideal_vespene_harvesters)
            if ideal_harvesters + 4 < len(self.bot.units(DRONE)):
                await self.insert_hatchery()

    async def insert_hatchery(self):
        pass  # drones just getting killed and blocking unit building
        # self.bot.build_order_list.insert(0, HATCHERY)
        # if self.build_log_level_hatch:
            # print("insert hatchery into build")
            # print("build order list: {}".format(self.bot.build_order_list))

    async def need_hatchery_check(self):
        if self.build_log_level_hatch:
            print(f"id min ha = {self.ideal_mineral_harvesters}", end=" ")
            print(f"id ves ha = {self.ideal_vespene_harvesters}", end=" ")
            print(f"drones + 8 = {len(self.bot.units(DRONE)) + 8}")
        if (self.ideal_mineral_harvesters + self.ideal_vespene_harvesters
            <  len(self.bot.units(DRONE)) + 8
        ):
            await self.moving_to_hatchery_check()

    async def moving_to_hatchery_check(self):
        #if self.bot.time > 400:
        #    print("moving to hatch routine")
        if not self.bot.worker_moving_to_hatchery:
            await self.pick_expansion_location()
            await self.pick_worker_for_hatchery()
            if self.bot.worker_for_hatchery:
                self.bot.worker_for_hatchery.move(self.bot.next_hatchery_position)
                self.bot.worker_moving_to_hatchery = True
                # print("moving to hatchery: ")

        if self.bot.worker_moving_to_hatchery:
            # print("moving to hatchery: " + str(self.bot.minerals) + " " + str(len(self.bot.units(DRONE))))
            if self.bot.minerals >= 300:
                # if self.bot.worker_for_hatchery.position near self.bot.next_hatchery_position
                if await self.bot.can_place(HATCHERY, self.bot.next_hatchery_position):
                    await self.start_hatchery()

    async def pick_worker_for_hatchery(self):
        for d in self.bot.units(DRONE):
            if (d.tag in self.bot.mineral_drone_tags
                and not d.is_carrying_resource
            ):
                self.bot.worker_for_hatchery = d
                self.bot.hatchery_drone_tags.add(d.tag)
                self.bot.mineral_drone_tags.remove(d.tag)
                break

    async def pick_expansion_location(self):
        closest = None
        distance = math.inf
        for el in self.bot.open_expansions:
            # if any(map(is_near_to_expansion, self.bot.townhalls)):
                # already taken
                # print('taken')
                # quit()
                # continue
            startp = self.bot._game_info.player_start_location
            d = await self.bot._client.query_pathing(startp, el)
            if d is None:
                continue
            if d < distance:
                distance = d
                closest = el

        if closest:
            self.bot.next_hatchery_position =  closest
            print("\nnext_hatchery_position")
            print(self.bot.next_hatchery_position)
            await self.update_open_expansions()

    async def update_open_expansions(self):
        self.bot.open_expansions_new = []
        for el in self.bot.open_expansions:
            if self.is_near_expansion(el, self.bot.next_hatchery_position):
                # print("we have a match")
                # print('expansion')
                # print(el)
                continue
            else:
                self.bot.open_expansions_new.append(el)

        # print(len(self.bot.open_expansions))
        self.bot.open_expansions = self.bot.open_expansions_new
        # for num, el in enumerate(self.bot.open_expansions):
            # print(list(self.bot.open_expansions)[num])
        # quit()
        # print(len(self.bot.open_expansions))
        # quit()

    def is_near_expansion(self, el, t):
        return t.position.distance_to(el) < self.bot.EXPANSION_GAP_THRESHOLD

    async def start_hatchery(self):
        self.bot.worker_for_hatchery.build(HATCHERY, self.bot.next_hatchery_position)
        if self.build_log_level_hatch:
            print( "hatchery started ", end="")
            print(self.bot.next_hatchery_position)

        self.bot.worker_moving_to_hatchery = False

        if len(self.bot.build_order_list) > 0:
            if self.bot.build_order_list[0] == HATCHERY:
                self.bot.build_order_list.pop(0)
                # await self.print_build_order_list()
        # self.bot.write_data.start_hatchery()

    async def baneling_nest_check(self):
        build_baneling_nest_flag = False

        if (self.bot.minerals >= 100  and self.bot.vespene >= 50
        and len(self.bot.units(DRONE)) > 0):
            if (self.hatch_count < 1
            or not self.bot.structures(SPAWNINGPOOL).ready.exists):
                pass
            elif len(self.bot.build_order_list) > 0:
                if self.bot.build_order_list[0] == BANELINGNEST:
                    build_baneling_nest_flag = True
            else:
                if self.bot.queen_started and not self.bot.structures(BANELINGNEST).ready.exists:
                    if not self.bot.already_pending(BANELINGNEST):
                        build_baneling_nest_flag = True

        if build_baneling_nest_flag:
            for d in range(4, 15):
                self.pos = self.bot.hatchery.position.to2.towards(self.bot.game_info.map_center, d)

                if await self.bot.can_place(BANELINGNEST, self.pos):
                    await self.start_baneling_nest()

    async def start_baneling_nest(self):
        drone = self.bot.workers.closest_to(self.pos)
        started = drone.build(BANELINGNEST, self.pos)
        if not started:
            return
        print('start baneling nest')

        if len(self.bot.build_order_list) > 0:
            if self.bot.build_order_list[0] == BANELINGNEST:
                self.bot.build_order_list.pop(0)
#                await self.print_build_order_list()
#        self.bot.write_data.start_baneling_nest()

    async def roach_warren_check(self):
        if (self.bot.structures(ROACHWARREN).exists
        or self.bot.already_pending(ROACHWARREN)
        or not self.build_roach_warren
        ):
            return

        if (self.bot.minerals >= 150 and
            len(self.bot.units(DRONE)) > 0 and
            self.bot.structures(SPAWNINGPOOL).ready.exists
        ):
            if (len(self.bot.build_order_list) > 0 and
                self.bot.build_order_list[0] == ROACHWARREN
            ):
                await self.pick_roach_warren_location()
            else:
                await self.strat_roach_warren()

    async def strat_roach_warren(self):
        if not self.bot.queen_started:
            return

        if (self.bot.structures(EVOLUTIONCHAMBER).exists or
            self.bot.already_pending(EVOLUTIONCHAMBER) or
            self.bot.proxy_pylon or
            self.bot.proxy_rush_flag
        ):
            await self.pick_roach_warren_location()
        elif (len(self.bot.units(DRONE)) >= 30 or
            self.bot.minerals >= 600
        ):
            await self.pick_roach_warren_location()

    async def pick_roach_warren_location(self):
        for h in self.bot.zerg_towns_ready:
            self.near = h.position
            await self.pick_build_location_1()

            for self.r in range(4, 15):
                await self.pick_build_location_2()

                if await self.bot.can_place(ROACHWARREN, self.target):
                    await self.start_roach_warren()
                    return

    async def start_roach_warren(self):
        await self.pick_drone_to_build()
        # drone = self.bot.workers.closest_to(self.target)
        # drone.build(ROACHWARREN, self.target)
        if self.d:
            self.d.build(ROACHWARREN, self.target)
            print('start roach warren')

            if (len(self.bot.build_order_list) > 0 and
                self.bot.build_order_list[0] == ROACHWARREN
            ):
                self.bot.build_order_list.pop(0)
                # await self.print_build_order_list()
        # self.bot.write_data.start_roach_warren()

    async def roach_speed_check(self):
        if self.bot.roach_speed_started:
            return

        if (self.bot.minerals >= 100 and self.bot.vespene >= 100
        and self.bot.structures(ROACHWARREN).ready
        and self.bot.structures(LAIR).ready):
            await self.start_roach_speed()
            # await self.move_workers_from_first_gas_check()

    async def start_roach_speed(self):
        rw = self.bot.structures(ROACHWARREN).ready
        rw.first(RESEARCH_GLIALREGENERATION)
        self.bot.roach_speed_started = True
        print('start roach_speed')
        if len(self.bot.build_order_list) > 0:
            if self.bot.build_order_list[0] == ROACH_SPEED:
                self.bot.build_order_list.pop(0)
        # self.bot.write_data.print_roach_speed()

    async def lair_check(self):
        # build_lair_flag = False
        if (self.bot.minerals >= 150 and self.bot.vespene > 100
            and self.hatch_count > 0
            and self.bot.structures(SPAWNINGPOOL).ready.exists
        ):
            if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == LAIR):
                await self.build_order_lair_check()
            else:
                await self.strat_lair_check()

    async def build_order_lair_check(self):
        if len(self.bot.hatcheries) > 0:
            await self.build_lair()

    async def strat_lair_check(self):
        if (self.bot.queen_started
            and not self.bot.already_pending(LAIR)
            and not self.bot.structures(LAIR).ready
            and not self.bot.structures(HIVE).ready
            and len(self.bot.zerg_towns) > 1
            # and len(self.bot.units(LARVA)) == 0
            and len(self.bot.units(DRONE)) > 30
        ):
            await self.build_lair()

    async def build_lair(self):
        for h in self.bot.hatcheries:
            if h.is_idle:
                h.build(LAIR)
                await self.start_lair()
                return

    async def start_lair(self):
        # self.bot.write_data.start_lair()
        if self.build_log_level:
            print(f"")
            print('start lair')

        if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == LAIR
        ):
            self.bot.build_order_list.pop(0)
            # await self.print_build_order_list()

    async def hive_check(self):
        if (self.bot.minerals >= 200 and self.bot.vespene > 150
            and self.bot.structures(LAIR).ready.exists
            and self.bot.structures(INFESTATIONPIT).ready.exists
        ):
            if (len(self.bot.build_order_list) > 0
                and self.bot.build_order_list[0] == HIVE
            ):
                await self.pick_hive_location()
            else:
                await self.strat_hive_check()

    async def strat_hive_check(self):
        if not self.build_hive:
            return
        if (not self.bot.already_pending(HIVE)
            and not self.bot.structures(HIVE).ready.exists
            and len(self.bot.units(DRONE)) > 60
        ):
            await self.pick_hive_location()

    async def pick_hive_location(self):
        for h in self.bot.structures(LAIR):
            if h.is_idle:
                h.build(HIVE)
                await self.start_hive()
                return

    async def start_hive(self):
        # self.bot.write_data.start_lair()
        print('start hive')

        if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == HIVE
        ):
            self.bot.build_order_list.pop(0)
            # await self.print_build_order_list()

    async def research_burrow_check(self):
        if (self.bot.structures(LAIR).ready and
            self.bot.can_afford(AbilityId.RESEARCH_BURROW) and
            self.bot.already_pending_upgrade(UpgradeId.BURROW) == 0
        ):
            if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == RESEARCH_BURROW):
                await self.research_burrow()
            elif self.bot.build_order == BuildOrderId.ZVT_ANTI_A_1:
                await self.research_burrow()

    async def research_burrow(self):
        for lair in self.bot.structures(LAIR).ready:
            if lair.is_idle:
                print("research burrow")
                lair(AbilityId.RESEARCH_BURROW)
                if (len(self.bot.build_order_list) > 0
                    and self.bot.build_order_list[0] == RESEARCH_BURROW
                ):
                    self.bot.build_order_list.pop(0)
                    print("build order list: {}".format(self.bot.build_order_list))
                else:
                    print("foo")
                return

    async def no_evo(self):
        self.evo_flag = False
        if (len(self.bot.build_order_list) > 0 and
            self.bot.build_order_list[0] == NOEVO
        ):
            self.bot.build_order_list.pop(0)

    async def evo(self):
        self.evo_flag = True
        if (len(self.bot.build_order_list) > 0 and
            self.bot.build_order_list[0] == EVO
        ):
            self.bot.build_order_list.pop(0)

    async def evo_chamber_check(self):
        if (self.bot.minerals >= 75 and
            len(self.bot.units(DRONE)) > 0 and
            self.bot.hatcheries.amount > 0
        ):
            if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == EVOLUTIONCHAMBER):
                await self.build_evo_chamber()
            else:
                await self.strat_evo_chamber()

    async def strat_evo_chamber(self):
        # early code - build one evo chamber and stops
        if (self.bot.structures(EVOLUTIONCHAMBER).exists or
            self.bot.already_pending(EVOLUTIONCHAMBER) or
            not self.evo_flag
        ):
            return

        if (self.bot.structures(SPAWNINGPOOL).ready.exists
        and self.bot.queen_started
        and len(self.bot.units(EVOLUTIONCHAMBER)) < 1):
            await self.build_evo_chamber()

    async def build_evo_chamber(self):
        for d in range(4, 15):
            self.pos = self.bot.hatchery.position.to2.towards(self.bot.game_info.map_center, d)
            if await self.bot.can_place(EVOLUTIONCHAMBER, self.pos):
                await self.start_evo_chamber()
                return

    async def start_evo_chamber(self):
        if len(self.bot.units(DRONE)) > 0:
            drone = self.bot.workers.closest_to(self.pos)
            drone.build(EVOLUTIONCHAMBER, self.pos)
            # self.bot.write_data.start_evo_chamber()
            print('start evolution_chamber')

            if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == EVOLUTIONCHAMBER):
                self.bot.build_order_list.pop(0)
                # await self.print_build_order_list()

    async def evolution_upgrade_check(self):
        if (len(self.bot.build_upgrade_list) > 0 and
            self.bot.can_afford(UpgradeId.ZERGMELEEWEAPONSLEVEL1)
        ):
            upgrade_next = self.bot.build_upgrade_list[0]
        else:
            return

        evolution_chambers = self.bot.structures(UnitTypeId.EVOLUTIONCHAMBER)
        if evolution_chambers:
            for evolution_chamber in evolution_chambers.ready:
                # print(f"upgrade next {upgrade_next}")
                if not evolution_chamber.is_idle:
                    # print(f"Time: {self.bot.time_formatted} ", end="")
                    # print('evolution chamber is busy')
                    pass
                elif (upgrade_next == ZERGMELEEWEAPONSLEVEL1 and
                    self.bot.already_pending_upgrade(UpgradeId.ZERGMELEEWEAPONSLEVEL1) < 1 and
                    self.bot.can_afford(UpgradeId.ZERGMELEEWEAPONSLEVEL1)
                ):
                    await self.start_melee_upgrade_1(evolution_chamber)
                elif (upgrade_next == ZERGMISSILEWEAPONSLEVEL1 and
                    self.bot.already_pending_upgrade(UpgradeId.ZERGMISSILEWEAPONSLEVEL1) < 1 and
                    self.bot.can_afford(UpgradeId.ZERGMISSILEWEAPONSLEVEL1)
                ):
                    await self.start_missile_upgrade_1(evolution_chamber)
                elif (upgrade_next == ZERGGROUNDARMORSLEVEL1 and
                    self.bot.already_pending_upgrade(UpgradeId.ZERGGROUNDARMORSLEVEL1) < 1 and
                    self.bot.can_afford(UpgradeId.ZERGGROUNDARMORSLEVEL1)
                ):
                    await self.start_ground_armor_1(evolution_chamber)
                elif ((self.bot.structures(LAIR).ready.exists or
                    self.bot.structures(HIVE).ready.exists) and
                    (upgrade_next == ZERGMELEEWEAPONSLEVEL2 or
                    upgrade_next == ZERGMISSILEWEAPONSLEVEL2 or
                    upgrade_next == ZERGGROUNDARMORSLEVEL2)
                ):
                    await self.upgrade_level_2(evolution_chamber, upgrade_next)
                elif (self.bot.structures(HIVE).ready.exists and
                    (upgrade_next == ZERGMELEEWEAPONSLEVEL3 or
                    upgrade_next == ZERGMISSILEWEAPONSLEVEL3 or
                    upgrade_next == ZERGGROUNDARMORSLEVEL3)
                ):
                    await self.upgrade_level_3(evolution_chamber, upgrade_next)

    async def upgrade_level_2(self, evolution_chamber, upgrade_next):
        if self.build_log_level:
            print(f"minerals {self.bot.minerals} vespene {self.bot.vespene}")
        if (upgrade_next == ZERGMELEEWEAPONSLEVEL2 and
            self.bot.can_afford(UpgradeId.ZERGMELEEWEAPONSLEVEL2)
        ):
            await self.start_melee_upgrade_2(evolution_chamber)
        elif (upgrade_next == ZERGMISSILEWEAPONSLEVEL2 and
            self.bot.can_afford(UpgradeId.ZERGMISSILEWEAPONSLEVEL2)
        ):
            await self.start_missile_upgrade_2(evolution_chamber)
        elif (upgrade_next == ZERGGROUNDARMORSLEVEL2 and
            self.bot.can_afford(UpgradeId.ZERGGROUNDARMORSLEVEL2)
        ):
            await self.start_ground_armor_2(evolution_chamber)

    async def upgrade_level_3(self, evolution_chamber, upgrade_next):
        if (upgrade_next == ZERGMELEEWEAPONSLEVEL3 and
            self.bot.can_afford(UpgradeId.ZERGMELEEWEAPONSLEVEL3)
        ):
            await self.start_melee_upgrade_3(evolution_chamber)
        elif (upgrade_next == ZERGMISSILEWEAPONSLEVEL3 and
            self.bot.can_afford(UpgradeId.ZERGMISSILEWEAPONSLEVEL3)
        ):
            await self.start_missile_upgrade_3(evolution_chamber)
        elif (upgrade_next == ZERGGROUNDARMORSLEVEL3 and
            self.bot.can_afford(UpgradeId.ZERGGROUNDARMORSLEVEL3)
        ):
            await self.start_ground_armor_3(evolution_chamber)

    async def start_melee_upgrade_1(self, evolution_chamber):
        started = evolution_chamber.research(UpgradeId.ZERGMELEEWEAPONSLEVEL1)
        if not started:
            return
        print(f"Time: {self.bot.time_formatted} ", end="")
        print('start melee upgrade 1')
        if len(self.bot.build_upgrade_list) > 0:
            if self.bot.build_upgrade_list[0] == ZERGMELEEWEAPONSLEVEL1:
                self.bot.build_upgrade_list.pop(0)
        # self.bot.write_data.print_hydra_speed()

    async def start_missile_upgrade_1(self, evolution_chamber):
        started = evolution_chamber.research(UpgradeId.ZERGMISSILEWEAPONSLEVEL1)
        if not started:
            return
        print(f"Time: {self.bot.time_formatted} ", end="")
        print('start missile upgrade 1')
        if len(self.bot.build_upgrade_list) > 0:
            if self.bot.build_upgrade_list[0] == ZERGMISSILEWEAPONSLEVEL1:
                self.bot.build_upgrade_list.pop(0)
        # self.bot.write_data.print_hydra_speed()

    async def start_ground_armor_1(self, evolution_chamber):
        started = evolution_chamber.research(UpgradeId.ZERGGROUNDARMORSLEVEL1)
        if not started:
            return
        print(f"Time: {self.bot.time_formatted} ", end="")
        print('start ground armor 1')
        if len(self.bot.build_upgrade_list) > 0:
            if self.bot.build_upgrade_list[0] == ZERGGROUNDARMORSLEVEL1:
                self.bot.build_upgrade_list.pop(0)
        # self.bot.write_data.print_hydra_speed()

    async def start_melee_upgrade_2(self, evolution_chamber):
        started = evolution_chamber.research(UpgradeId.ZERGMELEEWEAPONSLEVEL2)
        if not started:
            return
        print(f"Time: {self.bot.time_formatted} ", end="")
        print('start melee upgrade 2')
        if len(self.bot.build_upgrade_list) > 0:
            if self.bot.build_upgrade_list[0] == ZERGMELEEWEAPONSLEVEL2:
                self.bot.build_upgrade_list.pop(0)
        # self.bot.write_data.print_hydra_speed()

    async def start_missile_upgrade_2(self, evolution_chamber):
        started = evolution_chamber.research(UpgradeId.ZERGMISSILEWEAPONSLEVEL2)
        if not started:
            return
        print(f"Time: {self.bot.time_formatted} ", end="")
        print('start missile upgrade 2')
        if len(self.bot.build_upgrade_list) > 0:
            if self.bot.build_upgrade_list[0] == ZERGMISSILEWEAPONSLEVEL2:
                self.bot.build_upgrade_list.pop(0)
        # self.bot.write_data.print_hydra_speed()

    async def start_ground_armor_2(self, evolution_chamber):
        started = evolution_chamber.research(UpgradeId.ZERGGROUNDARMORSLEVEL2)
        if not started:
            return
        print(f"Time: {self.bot.time_formatted} ", end="")
        print('start ground armor 2')
        if len(self.bot.build_upgrade_list) > 0:
            if self.bot.build_upgrade_list[0] == ZERGGROUNDARMORSLEVEL2:
                self.bot.build_upgrade_list.pop(0)
        # self.bot.write_data.print_hydra_speed()

    async def start_melee_upgrade_3(self, evolution_chamber):
        started = evolution_chamber.research(UpgradeId.ZERGMELEEWEAPONSLEVEL3)
        if not started:
            return
        print(f"Time: {self.bot.time_formatted} ", end="")
        print('start melee upgrade 3')
        if len(self.bot.build_upgrade_list) > 0:
            if self.bot.build_upgrade_list[0] == ZERGMELEEWEAPONSLEVEL3:
                self.bot.build_upgrade_list.pop(0)
        # self.bot.write_data.print_hydra_speed()

    async def start_missile_upgrade_3(self, evolution_chamber):
        started = evolution_chamber.research(UpgradeId.ZERGMISSILEWEAPONSLEVEL3)
        if not started:
            return
        print(f"Time: {self.bot.time_formatted} ", end="")
        print('start missile upgrade 3')
        if len(self.bot.build_upgrade_list) > 0:
            if self.bot.build_upgrade_list[0] == ZERGMISSILEWEAPONSLEVEL3:
                self.bot.build_upgrade_list.pop(0)
        # self.bot.write_data.print_hydra_speed()

    async def start_ground_armor_3(self, evolution_chamber):
        started = evolution_chamber.research(UpgradeId.ZERGGROUNDARMORSLEVEL3)
        if not started:
            return
        print(f"Time: {self.bot.time_formatted} ", end="")
        print('start ground armor 3')
        if len(self.bot.build_upgrade_list) > 0:
            if self.bot.build_upgrade_list[0] == ZERGGROUNDARMORSLEVEL3:
                self.bot.build_upgrade_list.pop(0)
        # self.bot.write_data.print_hydra_speed()

    async def hydra_den_check(self):
        # build_hydra_den_flag = False
        if (self.build_hydralisk_den and
            self.bot.minerals >= 100 and self.bot.vespene >= 100
        and self.bot.structures(LAIR).ready.exists):
            if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == HYDRALISKDEN):
                await self.pick_hydra_den_location()
                #print('build order hydra den')
            else:
                if (self.bot.structures(HYDRALISKDEN).exists
                or self.bot.already_pending(HYDRALISKDEN)):
                    pass
                else:
                    #print('no build order hydra den pit')
                    await self.pick_hydra_den_location()

    async def pick_hydra_den_location(self):
        for h in (self.bot.hatcheries |
                self.bot.structures(LAIR)):
            for d in range(4, 15):
                self.pos = h.position.to2.towards(self.bot.game_info.map_center, d)
                if await self.bot.can_place(HYDRALISKDEN, self.pos):
                    await self.start_hydra_den()
                    return

    async def start_hydra_den(self):
        if len(self.bot.units(DRONE)) > 0:
            drone = self.bot.workers.closest_to(self.pos)
            drone.build(HYDRALISKDEN, self.pos)
            print('start hydralisk den')
            # self.bot.write_data.start_hydra_den()

            if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == HYDRALISKDEN):
                self.bot.build_order_list.pop(0)
                # await self.print_build_order_list()

    async def hydra_upgrade_check(self):
        hydra_dens = self.bot.structures(UnitTypeId.HYDRALISKDEN)
        if hydra_dens and (self.bot.minerals >= 100 and self.bot.vespene >= 100):
            for hydra_den in hydra_dens.ready.idle:
                if (self.bot.already_pending_upgrade(UpgradeId.EVOLVEGROOVEDSPINES) < 1
                and self.bot.can_afford(UpgradeId.EVOLVEGROOVEDSPINES)):
                    await self.start_hydra_range(hydra_den)
                elif (self.bot.already_pending_upgrade(UpgradeId.EVOLVEMUSCULARAUGMENTS) < 1
                and self.bot.can_afford(UpgradeId.EVOLVEMUSCULARAUGMENTS)):
                    await self.start_hydra_speed(hydra_den)

    async def start_hydra_speed(self, hydra_den):
        hydra_den.research(UpgradeId.EVOLVEMUSCULARAUGMENTS)
        print('start hydra_speed')
        if len(self.bot.build_order_list) > 0:
            if self.bot.build_order_list[0] == HYDRA_SPEED:
                self.bot.build_order_list.pop(0)
        # self.bot.write_data.print_hydra_speed()

    async def start_hydra_range(self, hydra_den):
        hydra_den.research(UpgradeId.EVOLVEGROOVEDSPINES)
        print('start hydra_range')
        if len(self.bot.build_order_list) > 0:
            if self.bot.build_order_list[0] == HYDRA_RANGE:
                self.bot.build_order_list.pop(0)
        # self.bot.write_data.print_hydra_range()

    async def lurker_den_check(self):
        if (self.bot.minerals >= 100 and self.bot.vespene >= 150
        and self.bot.structures(HYDRALISKDEN).ready.exists):
            if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == LURKERDENMP):
                await self.pick_lurker_den_location()
                #print('build order lurker den')
            else:
                await self.strat_lurker_den()

    async def strat_lurker_den(self):
        if (self.bot.structures(LURKERDENMP).exists
        or self.bot.already_pending(LURKERDENMP)):
            return

        if (self.bot.build_order == BuildOrderId.ONE_BASE_HYDRA or
            self.bot.build_order == BuildOrderId.TWO_BASE_HYDRA
        ):
            await self.pick_lurker_den_location()

    async def pick_lurker_den_location(self):
        for h in self.bot.zerg_towns_ready:
            self.near = h.position
            await self.pick_build_location_1()
            for self.r in range(4, 15):
                await self.pick_build_location_2()
                if await self.bot.can_place(LURKERDENMP, self.target):
                    await self.start_lurker_den()
                    return

    async def start_lurker_den(self):
        if len(self.bot.units(DRONE)) > 0:
            drone = self.bot.workers.closest_to(self.target)
            drone.build(LURKERDENMP, self.target)
            print('start lurker den')
            # self.bot.write_data.start_lurker_den()

            if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == LURKERDENMP):
                self.bot.build_order_list.pop(0)
                # await self.print_build_order_list()

    async def inf_pit_check(self):
        if (self.bot.minerals >= 100 and self.bot.vespene > 100 and
            self.bot.structures(LAIR).ready.exists
        ):
            if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == INFESTATIONPIT):
                await self.pick_inf_pit_location()
                # print('build order infestation pit')
            else:
                await self.strat_inf_pit()

    async def strat_inf_pit(self):
        if (self.build_infestation_pit and
            not self.bot.structures(INFESTATIONPIT).exists and
            not self.bot.already_pending(INFESTATIONPIT)
        ):
            await self.pick_inf_pit_location()
            # print('no build order infestation pit')

    async def pick_inf_pit_location(self):
        # for h in (self.bot.hatcheries |
                # self.bot.structures(LAIR)):
        for h in self.bot.zerg_towns_ready:
            self.near = h.position
            await self.pick_build_location_1()
            for self.r in range(4, 15):
                await self.pick_build_location_2()
                if await self.bot.can_place(INFESTATIONPIT, self.target):
                    await self.start_inf_pit()
                    return

    async def start_inf_pit(self):
        if len(self.bot.units(DRONE)) > 0:
            drone = self.bot.workers.closest_to(self.target)
            drone.build(INFESTATIONPIT, self.target)
            print('start infestation pit')
            # self.bot.write_data.start_inf_pit()

            if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == INFESTATIONPIT):
                self.bot.build_order_list.pop(0)
                # await self.print_build_order_list()

    async def spire_check(self):
        if (self.bot.minerals >= 200 and self.bot.vespene > 200 and
            (self.bot.structures(LAIR).ready.exists or
            self.bot.structures(HIVE).ready.exists) and
            len(self.bot.units(DRONE)) > 0
        ):
            if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == SPIRE):
                await self.pick_spire_location()
                # print('build order infestation pit')
            else:
                await self.strat_spire()

    async def strat_spire(self):
        if (self.bot.structures(SPIRE).exists or
            self.bot.already_pending(SPIRE) or
            self.bot.structures(GREATERSPIRE).exists
        ):
            return

        if (self.bot.build_order == BuildOrderId.ONE_BASE_BROODLORD or
            self.bot.build_order == BuildOrderId.TWO_BASE_BROODLORD or
            self.bot.structures(HIVE).ready.exists
        ):
            await self.pick_spire_location()
            # print('no build order spire')

    async def pick_spire_location(self):
        for h in self.bot.zerg_towns_ready:
            self.near = h.position
            await self.pick_build_location_1()
            for self.r in range(4, 15):
                await self.pick_build_location_2()
                if await self.bot.can_place(SPIRE, self.target):
                    await self.start_spire()
                    return

    async def start_spire(self):
        await self.pick_drone_to_build()
        # if len(self.bot.units(DRONE)) > 0:
            #drone = self.bot.workers.closest_to(self.target)
            # drone.build(SPIRE, self.target)
            # drone = self.bot.workers.closest_to(self.target)
            # drone.build(SPIRE, self.target)
        if self.d:
            self.d.build(SPIRE, self.target)
            print('start spire')
            # self.bot.write_data.start_spire()

            if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == SPIRE):
                self.bot.build_order_list.pop(0)
                # await self.print_build_order_list()

    async def pick_drone_to_build(self):
        distance = 20
        drones = self.bot.units.filter(lambda d:
            d.type_id == DRONE
            and d.distance_to(self.target) < distance
            and d.tag in self.bot.mineral_drone_tags
            and not d.is_carrying_resource)

        if drones:
            self.d = drones.closest_to(self.target)
            await self.update_building_drone_tags()
        else:
            self.d = None

    async def nydus_network_check(self):
        if (self.bot.minerals >= 150 and self.bot.vespene > 150
        and self.bot.structures(LAIR).ready.exists):
            if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == NYDUSNETWORK):
                await self.pick_nydus_network_location()
            elif (self.bot.build_order == BuildOrderId.ZVT_ANTI_A_1
                and not self.bot.structures(NYDUSNETWORK).ready
            ):
                await self.strat_build_nydus_network()

    async def strat_build_nydus_network(self):
        if not self.bot.structures(NYDUSNETWORK).ready:
            await self.pick_nydus_network_location()

    async def pick_nydus_network_location(self):
        self.near = self.bot.zerg_towns[0].position
        await self.pick_build_location_1()

        for self.r in range(4, 15):
            await self.pick_build_location_2()

            if await self.bot.can_place(NYDUSNETWORK, self.target):
                await self.start_nydus_network()

    async def start_nydus_network(self):
        if len(self.bot.units(DRONE)) > 0:
            drone = self.bot.workers.closest_to(self.target)
            drone.build(NYDUSNETWORK, self.target)
            print('start nydus network')
            # self.bot.write_data.start_nydus_network()

            if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == NYDUSNETWORK):
                self.bot.build_order_list.pop(0)
                # await self.print_build_order_list()

    async def nydus_canal_check(self):
        if (self.bot.minerals >= 75 and self.bot.vespene >= 75
            and self.bot.structures(NYDUSNETWORK).ready
        ):
            if (len(self.bot.build_order_list) > 0
            and self.bot.build_order_list[0] == NYDUSCANAL):
                await self.pick_nydus_canal_location()
            elif (self.bot.build_order == BuildOrderId.ZVT_ANTI_A_1
                and not self.bot.structures(NYDUSCANAL).ready
            ):
                await self.pick_nydus_canal_location()

    async def pick_nydus_canal_location(self):
        target_c = self.bot.away
        self.near = self.bot.units.closest_to(target_c).position
        print(f"canal vision position: {self.near}")

        #  self.near = self.bot.away
        await self.pick_nydus_location_1()

        for self.r in range(1, 12):
            await self.pick_nydus_location_2()

            if await self.bot.can_place(NYDUSCANAL, self.target):
                await self.start_nydus_canal()
                print(f"nydus canal {self.target}")

    async def pick_nydus_location_1(self):
        self.x_sign = math.copysign(1, self.bot.away.x - self.bot.home.x)
        self.y_sign = math.copysign(1, self.bot.away.y - self.bot.home.y)

    async def pick_nydus_location_2(self):
        self.r = 7 + self.r * 3.99 / 11
        if (self.bot.map_name == 'Deathaura LE' or
            self.bot.map_name == 'Disco Bloodbath LE' or
            self.bot.map_name == 'Triton LE'
        ):
            posx = (self.near.x + self.x_sign *
                    random.random() * math.sqrt(121 - self.r ** 2))
            posy = self.near.y + self.y_sign * self.r
        else:
            posx = self.near.x + self.x_sign * self.r
            posy = (self.near.y + self.y_sign *
                    random.random() * math.sqrt(121 - self.r ** 2))
        self.target = Point2((posx, posy))

    async def start_nydus_canal(self):
        unit = self.bot.structures(NYDUSNETWORK).first
        unit(AbilityId.BUILD_NYDUSWORM, self.target)
        print('start nydus canal')
        # self.bot.write_data.start_nydus_network()

        if (len(self.bot.build_order_list) > 0
        and self.bot.build_order_list[0] == NYDUSCANAL):
            self.bot.build_order_list.pop(0)
            # await self.print_build_order_list()

    async def greater_spire_check(self):
        if (self.bot.structures(HIVE).ready and
            self.bot.structures(SPIRE).ready and
            self.bot.can_afford(GREATERSPIRE) and not
            (self.bot.structures(GREATERSPIRE).ready or
            self.bot.already_pending(GREATERSPIRE))
        ):
            if (len(self.bot.build_order_list) > 0 and
                self.bot.build_order_list[0] == GREATERSPIRE
            ):
                await self.pick_greater_spire()
            else:
                await self.pick_greater_spire()

    async def pick_greater_spire(self):
        for self.spire in self.bot.structures(SPIRE).ready:
             if self.spire.is_idle:
                 await self.start_greater_spire()
                 return

    async def start_greater_spire(self):
        self.spire.build(GREATERSPIRE)
        if (len(self.bot.build_order_list) > 0
        and self.bot.build_order_list[0] == GREATERSPIRE):
            self.bot.build_order_list.pop(0)
            # await self.print_build_order_list()
