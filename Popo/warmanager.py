import random

from sc2.client import *
from sc2.constants import *
from sc2.units import Units
from sc2.helpers import ControlGroup

from .build_order_id import BuildOrderId

class WarManager():
    def __init__(self, bot=None):
        self.bot = bot
        self.bot.war_status = "Peace"
        self.prior_war_status = "Peace"

        self.enemy_units = None
        self.bot.all_in = False

        # worker rush data
        self.enemy_attacker_count = None
        self.close_attacker_count = None
        self.worker_rush_detected = False
        self.defend_the_worker_rush = False
        self.worker_rush_defended = False
        self.skip_other_logic = False
        self.all_in_for_game = False
        self.print_all_in = False  # Testing flag

        self.first_attack_flag = True  # Testing flag
        self.unit = None
        self.x = None
        self.y = None
        self.near = None
        self.target = None
        self.attackers = None
        self.workers_attacking = False

    async def Run(self):
        if self.bot.build_order == BuildOrderId.WORKER_RUSH:
            await self.attack_worker_rush()
        elif self.bot.time < 95 or self.worker_rush_detected:
            await self.worker_rush()
            if self.defend_the_worker_rush:
                await self.defend_worker_rush()
            elif self.worker_rush_defended:
                await self.just_kill_base()
            return
        else:
            await self.unit_attack()
            await self.attack_retreat()
            await self.base_status()
            if self.bot.war_status == "DefendBase":
                await self.defend_attack()

    async def unit_attack(self):
        await self.special_builds_and_defense()

        if self.skip_other_logic:
            return

        self.bot.defense_power = max(self.bot.enemy_power, self.bot.time * 2.5 + 15)
        self.bot.defense_power = min(self.bot.defense_power, 2400)

        if self.bot.supply_used > 193:
            self.bot.all_in = True
        elif self.bot.all_in and self.bot.supply_used > 168:
            self.bot.all_in = True
        else:
            self.bot.all_in = False

        if self.bot.build_order == BuildOrderId.DROPPER_LORD_14:
            await self.dropper_lord_attack()
        elif self.bot.build_order == BuildOrderId.ONE_BASE_FAST_ROACH:
            if self.all_in_for_game or self.bot.attack_power > 425:
                self.bot.all_in = True
                self.all_in_for_game = True
        elif (self.bot.build_order == BuildOrderId.PIG_SWARM and
            self.bot.enemy_race != Race.Zerg and
            self.bot.time < 420
        ):
            pass
        elif self.bot.build_order == BuildOrderId.ZERGLING_RUSH:
            if self.all_in_for_game or self.bot.attack_power > 275:
                self.bot.all_in = True
                self.all_in_for_game = True
        elif self.bot.build_order == BuildOrderId.BUILD_17_16_17:
            if self.all_in_for_game or self.bot.attack_power > 500:
                self.bot.all_in = True
                self.all_in_for_game = True
        elif (self.bot.attack_power > self.bot.defense_power or
            self.bot.attack_power > 2500
        ):
            if self.bot.time > 300 and len(self.bot.enemy_structures) == 0:
                await self.clean_up()
            elif (self.bot.build_order == BuildOrderId.THREE_BASE_ROACH_01 or
                self.bot.build_order == BuildOrderId.ONE_BASE_HYDRA or
                self.bot.build_order == BuildOrderId.TWO_BASE_HYDRA
            ):
                if self.bot.time > 420:
                    self.bot.all_in = True
            else:
                self.bot.all_in = True

    async def attack_retreat(self):
        if self.bot.all_in:
            await self.all_in_attack()
        elif self.bot.build_order == BuildOrderId.ZERGLING_RUSH:
            pass
        else:  # retreat
            await self.retreat()

    async def special_builds_and_defense(self):
        self.skip_other_logic = True
        if (self.bot.build_order == BuildOrderId.TWELVE_POOL or
            self.bot.build_order == BuildOrderId.TWELVE_POOL_2 or
            self.bot.build_order == BuildOrderId.TWELVE_POOL_3 or
            self.bot.build_order == BuildOrderId.ZVT_ANTI_P_1
        ):
            await self.war_12_pool()
        elif self.bot.roach_rush_flag:
            await self.roach_rush()
        elif self.bot.build_order == BuildOrderId.ZVT_ANTI_A_1:
            await self.nydus_rush()
        else:
            self.skip_other_logic = False

    async def roach_rush(self):
        pass

    async def attack_worker_rush(self):
        if not self.workers_attacking:
            self.attackers = self.bot.units(DRONE)
            self.workers_attacking = True
        target = self.bot.enemy_structures.random_or(self.bot.enemy_start_locations[0]).position
        for d in self.attackers:
            d.attack(target)


    async def war_12_pool(self):
        self.bot.all_in = True
        self.x = 2/3
        self.y = 2/3

        if self.bot.time > 300 and len(self.bot.enemy_structures) == 0:
            await self.clean_up()
            return

        # target = self.bot.enemy_structures.random_or(self.bot.enemy_start_locations[0]).position
        for self.unit in self.bot.units.filter(lambda w:
            w.type_id in [UnitTypeId.ZERGLING] or
            w.type_id in [UnitTypeId.BANELING] or
            w.type_id in [UnitTypeId.ROACH] or
            w.type_id in [UnitTypeId.HYDRALISK] or
            w.type_id in [UnitTypeId.LURKERMP] or
            w.type_id in [UnitTypeId.CORRUPTOR] or
            w.type_id in [UnitTypeId.BROODLORD]
        ):
            # unit.attack(target)
            await self.attack_target()
            # testing 0.4.1.8
            if self.first_attack_flag:
                print(f"Time: {self.bot.time_formatted} ", end="")
                print("first attack - 12 pool")
                self.first_attack_flag = False

    async def nydus_rush(self):
        if self.bot.structures(NYDUSNETWORK).ready:
            self.move_into_nydus()
        if self.bot.structures(NYDUSCANAL).ready:
            self.unload_from_worm()
            self.nydus_attack()

    def move_into_nydus(self):
        target = self.bot.structures(NYDUSNETWORK).first
        target(AbilityId.LOAD_NYDUSNETWORK)
        for unit in self.bot.units.filter(lambda w:
            w.type_id in [UnitTypeId.ZERGLING] or
            w.type_id in [UnitTypeId.BANELING] or
            w.type_id in [UnitTypeId.ROACH] or
            w.type_id in [UnitTypeId.HYDRALISK] or
            w.type_id in [UnitTypeId.LURKERMP] or
            w.type_id in [UnitTypeId.QUEEN]
        ):
            if unit.distance_to(target) < 20:
                unit.smart(target)

    def unload_from_worm(self):
        target = self.bot.structures(NYDUSCANAL).first
        target(AbilityId.UNLOADALL_NYDUSWORM)

    def nydus_attack(self):
        for unit in self.bot.units.filter(lambda w:
            w.type_id in [UnitTypeId.ZERGLING] or
            w.type_id in [UnitTypeId.BANELING] or
            w.type_id in [UnitTypeId.ROACH] or
            w.type_id in [UnitTypeId.QUEEN]
        ):
            if (unit.distance_to(self.bot.away) <
                unit.distance_to(self.bot.home)
            ):
                if len(self.bot.enemy_structures) > 0:
                    target = self.bot.enemy_structures.closest_to(unit)
                else:
                    target = self.bot.away
                unit.attack(target)

    async def dropper_lord_attack(self):
        if (len(self.bot.units(ZERGLING)) >= 6
            and len(self.bot.units(OVERLORD)) >= 1
        ):
            for overlord in self.bot.units(OVERLORD):
                abilities = await self.bot.get_available_abilities(overlord)
                if AbilityId.LOAD_OVERLORD in abilities:
                    return
                await self.morph_dropper_lord()

        if len(self.bot.units(ZERGLING)) >= 6:
            await self.move_zerglings_to_dropper_lord()

    async def morph_dropper_lord(self):
        d_lord = self.bot.units(OVERLORD).closest_to(self.bot.away)
        print('d_lor position: ', d_lord.position)
        d_lord(AbilityId.MORPH_OVERLORDTRANSPORT)

    async def move_zerglings_to_dropper_lord(self):
        for overlord in self.bot.units(OVERLORD):
            abilities = await self.bot.get_available_abilities(overlord)
            if AbilityId.LOAD_OVERLORD in abilities:
                target = overlord.position
                zerglings = 0
                for unit in self.bot.units(ZERGLING):
                    unit.move(target)
                    zerglings += 1
                    if zerglings >= 8:
                        break

    async def all_in_attack(self):
        self.bot.war_status = 'War'
        if self.print_all_in:
            print(f"Time: {self.bot.time_formatted} ", end="")
            print("all in")
            self.print_all_in = False
        self.x = 2/3
        self.y = 2/3
        if self.bot.enemy_structures:
            target = self.bot.enemy_structures.closest_to(self.bot.home)
        else:
            target = self.bot.enemy_start_locations[0].position

        for self.unit in self.bot.units(ZERGLING).idle:
            await self.zergling_attack()
        for self.unit in self.bot.units(BANELING).idle:
            await self.baneling_attack()
        for self.unit in self.bot.units(BANELING).idle:
            await self.attack_target()
        for self.unit in self.bot.units(ROACH).idle:
            await self.attack_target()
        for self.unit in self.bot.units(HYDRALISK).idle:
            await self.attack_target()
        for self.unit in self.bot.units(LURKERMP).idle:
            await self.attack_target()
        for self.unit in self.bot.units(CORRUPTOR).idle:
            await self.attack_target()
        for self.unit in self.bot.units(BROODLORD).idle:
            await self.attack_target()

        for sh in self.bot.units(SWARMHOSTMP):
            target = self.bot.enemy_structures.closest_to(sh)
            target = target.position * 2/3 + sh.position * 1/3
            sh.move(target)
        await self.spawn_locusts_check()
        # testing 0.4.1.8
        if self.first_attack_flag:
            print(f"Time: {self.bot.time_formatted} ", end="")
            print("first attack - all in")
            self.first_attack_flag = False

    async def retreat(self):
        target = self.bot.rushwait_location # 0.3.1
        for unit in self.bot.units(SWARMHOSTMP):
            unit.move(target)

        for unit in self.bot.units.filter(lambda w:
            w.type_id in [UnitTypeId.ZERGLING] or
            w.type_id in [UnitTypeId.BANELING] or
            w.type_id in [UnitTypeId.ROACH] or
            w.type_id in [UnitTypeId.HYDRALISK] or
            w.type_id in [UnitTypeId.LURKERMP]
        ):
            unit.attack(target)
            # testing 0.4.1.8
            if self.first_attack_flag:
                print(f"Time: {self.bot.time_formatted} ", end="")
                print("first attack - unit attack")
                self.first_attack_flag = False

    async def desperation_check(self):
        if (self.bot.units(HATCHERY).amount == 0
        and self.bot.units(LAIR).amount == 0
        and self.bot.units(HIVE).amount == 0):
            await self.desperation()

    async def desperation(self):
        for unit in (self.bot.workers | self.bot.units(ZERGLING).idle
        | self.bot.units(BANELING).idle | self.bot.units(ROACH).idle
        | self.bot.units(HYDRALISK).idle | self.bot.units(LURKERMP).idle
        | self.bot.units(QUEEN).idle):
            target = self.bot.enemy_structures.random_or(self.bot.enemy_start_locations[0]).position
            unit.attack(target)
            # testing 0.4.1.8
            if self.first_attack_flag:
                print(f"Time: {self.bot.time_formatted} ", end="")
                print("first attack - desperation")
                self.first_attack_flag = False

    async def clean_up(self):
        for hy in self.bot.units(HYDRALISK).idle:
            hy.attack(random.choice(list(self.bot.expansion_locations)))

        for hy in self.bot.units(LURKERMP).idle:
            hy.attack(random.choice(list(self.bot.expansion_locations)))

        for ro in self.bot.units(ROACH).idle:
            ro.attack(random.choice(list(self.bot.expansion_locations)))

        for zl in self.bot.units(ZERGLING).idle:
            zl.attack(random.choice(list(self.bot.expansion_locations)))

        for zl in self.bot.units(BANELING).idle:
            zl.attack(random.choice(list(self.bot.expansion_locations)))

        for ol in self.bot.units(OVERLORD).idle:
            ol.move(random.choice(list(self.bot.expansion_locations)))

        for sh in self.bot.units(SWARMHOSTMP).idle:
            sh.move(random.choice(list(self.bot.expansion_locations)))
        await self.spawn_locusts_check()
        # testing 0.4.1.8
        if self.first_attack_flag:
            print(f"Time: {self.bot.time_formatted} ", end="")
            print("first attack - clean up")
            self.first_attack_flag = False

    async def worker_rush(self):
        if self.bot.time > 300:
            self.worker_rush_defended = True
            return
        hqlist = self.bot.structures(self.bot.townhall)
        hqpositions = []
        for n in hqlist:
            hqpositions.append(n.position)

        self.enemy_units = self.bot.enemy_units.filter(lambda w:
            w.type_id in [UnitTypeId.DRONE]
            or w.type_id in [UnitTypeId.ZERGLING]
            or w.type_id in [UnitTypeId.SCV]
            or w.type_id in [UnitTypeId.PROBE])

        self.enemy_attacker_count = 0
        self.close_attacker_count = 0
        for e in self.enemy_units:
            for n in hqpositions:
                distance2hq = e.position.distance_to(n)
                if distance2hq <= 40:
                    #self.enemy_attackers.append(e)
                    self.enemy_attacker_count +=1
                if distance2hq <= 9:
                    self.close_attacker_count +=1

        #if (self.bot.time % 2) < 0.36:
        #    print("enemies: " + str(self.enemy_attacker_count) + " ", end='')

        self.prior_war_status = self.bot.war_status

        if (self.enemy_attacker_count > 4
            and not self.worker_rush_detected):
            self.worker_rush_prep()
        elif self.enemy_attacker_count <= 1:
            self.bot.war_status = "Peace"
        if (self.close_attacker_count > 4
                and not self.defend_the_worker_rush):
            self.defend_the_worker_rush = True
            print("defend worker rush")

    def worker_rush_prep(self):
        if not self.worker_rush_detected:
            print("worker rush detected")
            self.worker_rush_detected = True
        self.bot.build_order_list = []
        drones_to_make = self.bot.minerals % 50
        for i in range(drones_to_make):
            self.bot.build_order_list.insert(0, DRONE)

    async def defend_worker_rush(self):
        defenders = self.bot.units.not_structure.ready.filter(lambda w:
            w.type_id not in [UnitTypeId.LARVA]
            and w.type_id not in [UnitTypeId.EGG])
        d_max = min(len(defenders), self.enemy_attacker_count + 2)
        # print(" defenders: " + str(d_max))
        # for d in range (0, d_max):
        for d in defenders:
            if len(self.enemy_units) > 0:
                target = self.enemy_units.closest_to(d.position)
                # target = self.enemy_attackers.closest_to(d.position)
            else:
                target = self.bot.enemy_structures.random_or(self.bot.enemy_start_locations[0]).position
                self.worker_rush_defended = True
            d.attack(target)
            # testing 0.4.1.8
            if self.first_attack_flag:
                print(f"Time: {self.bot.time_formatted} ", end="")
                print("first attack - defend worker rush")
                self.first_attack_flag = False

    async def base_status(self):
        hqlist = self.bot.structures(self.bot.townhall)
        hqpositions = []
        for n in hqlist:
            hqpositions.append(n.position)

        #self.enemy_attackers = []
        self.enemy_attacker_count = 0
        attackers = self.bot.enemy_units.not_structure.filter(
        lambda w: w.type_id not in [UnitTypeId.LARVA]
              and w.type_id not in [UnitTypeId.EGG]
              and w.type_id not in [UnitTypeId.DRONE]
              and w.type_id not in [UnitTypeId.OVERLORD]
              and w.type_id not in [UnitTypeId.PROBE]
              and w.type_id not in [UnitTypeId.SCV]
              )

        distance = await self.distance_to_defend()
        for att in attackers:
            for n in hqpositions:
                distance2hq = att.position.distance_to(n)
                if distance2hq <= distance:
                    #self.enemy_attackers.append(att)
                    self.enemy_attacker_count +=1

        self.prior_war_status = self.bot.war_status
        if self.enemy_attacker_count > 0:
            self.bot.war_status = "DefendBase"
        elif self.bot.all_in:
            self.bot.war_status = "War"
        else:
            self.bot.war_status = "Peace"

        if self.bot.war_status != "Peace" and (self.bot.time % 2) == 0:
            print("enemies: " + str(self.enemy_attacker_count) + " ", end='')

    async def distance_to_defend(self):
        if self.bot.zergling_rush or self.bot.roach_rush_flag:
            return 14
        else:
            return 32

    async def defend_attack(self):
        defenders = self.bot.units.not_structure.ready.filter(
        lambda w: w.type_id not in [UnitTypeId.LARVA]
            and w.type_id not in [UnitTypeId.EGG]
            and w.type_id not in [UnitTypeId.BANELINGCOCOON]
            and w.type_id not in [UnitTypeId.DRONE]
            and w.type_id not in [UnitTypeId.SWARMHOSTMP]
            and w.type_id not in [UnitTypeId.QUEEN]
            and w.type_id not in [UnitTypeId.OVERLORD])
        d_max = min(len(defenders), self.enemy_attacker_count + 2)
        # print(" defenders: " + str(d_max))
        # for d in range (0, d_max):
        for d in defenders:
            # target = random.choice(self.bot.enemy_units).position
            # target = random.choice(self.enemy_attackers).position
            target = self.bot.enemy_units.closest_to(d.position)
            d.attack(target)
            # testing 0.4.1.8
            if self.first_attack_flag:
                print(f"Time: {self.bot.time_formatted} ", end="")
                print("first attack - defend attack")
                self.first_attack_flag = False

        await self.spawn_locusts_check()

    async def just_kill_base(self):
        # After worker rush defended, just kill base
        self.bot.build_order_list = []
        target = self.bot.enemy_structures.random_or(self.bot.enemy_start_locations[0]).position
        for d in self.bot.units(DRONE):
            d.attack(target)
            # testing 0.4.1.8
            if self.first_attack_flag:
                print(f"Time: {self.bot.time_formatted} ", end="")
                print("first attack - just kill base")
                self.first_attack_flag = False

        for ol in self.bot.units(OVERLORD).idle:
            ol.move(random.choice(list(self.bot.expansion_locations)))


    async def spawn_locusts_check(self):
        for self.swarm_host in self.bot.units(SWARMHOSTMP).idle:
            abilities = await self.bot.get_available_abilities(self.swarm_host)
            if (AbilityId.EFFECT_SPAWNLOCUSTS in abilities
            and len(self.bot.enemy_units) >0):
                self.enemy = self.bot.enemy_units.closest_to(self.swarm_host.position)
                if self.swarm_host.distance_to(self.enemy) < 12:
                    await self.spawn_locusts()

    async def spawn_locusts(self):
        self.swarm_host(EFFECT_SPAWNLOCUSTS, self.enemy.position)
        self.bot.write_data.print_spawn_locusts()
        r = .65 + random.random() *.02
        # target = self.bot.rushwait_location * r + (1-r) * self.swarm_host.position
        target = self.bot.ramp_location
        self.swarm_host.move(target)

    async def calculate_target(self):
        if self.bot.enemy_structures:
            target = self.bot.enemy_structures.closest_to(self.unit.position).position
        else:
            target = self.bot.enemy_start_locations[0].position

        posx = (self.unit.position[0] + self.x * (target[0] - self.unit.position[0]))
        posy = (self.unit.position[1] + self.y * (target[1] - self.unit.position[1]))

        self.target = Point2((posx, posy))

    async def zergling_attack(self):
        await self.calculate_target()
        self.unit.attack(self.target)

    async def baneling_attack(self):
        await self.calculate_target()
        self.unit.attack(self.target)

    async def attack_target(self):
        await self.calculate_target()
        if self.bot.units(LURKERMP):
            await self.lurker_attack()

        self.unit.attack(self.target)

    async def lurker_attack(self):
        if self.unit.distance_to(self.target) <= 9:
            self.unit(AbilityId.BURROWDOWN_LURKER)
