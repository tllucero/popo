from sc2.client import *
import random
import os

class GameManager():
    def __init__(self, bot=None):
        self.bot = bot
        self.bot.microActions = []
        self.ability = self.bot.techtree.techtree['Ability']
        self.unit = self.bot.techtree.techtree['Unit']

        self.bot.unitInfo = {}
        self.bot.minerals_previous: int = 0

        # Race/Enemy specific
        self.bot.race_self = None
        self.bot.race_enemy = None
        self.bot.enemy_id = None
        self.bot.worker = None
        self.bot.townhall = None
        self.bot.gasbuilding = None

        # Locations
        self.bot.hq_location = None         # 1st townhall
        self.bot.ramp_location = None       # ramp to natural
        self.bot.rushwait_location = None   # position at natural
        self.bot.proxy_location = None      # proxy position near enemy start position
        self.bot.startpoint = None
        self.bot.mapcorner_sw = None
        self.bot.mapcorner_se = None
        self.bot.mapcorner_nw = None
        self.bot.mapcorner_ne = None
        self.bot.map_center_n = None
        self.bot.map_center_s = None
        self.bot.map_center_w = None
        self.bot.map_center_e = None
        self.bot.map_center = None

    def clearscreen(self):
        if os.name == 'nt':
            _ = os.system('cls')
        else:
            _ = os.system('clear')

    async def startGame(self):
        # Set ticks per game step
        self.bot._client.game_step = 4
        #self.bot.firstexpansion = await self.bot.get_next_expansion()

        # Set Positions
#        await self.set_positions()
        await self.mapbounds()
        await self.set_positions()

        self.bot.open_expansions = []
        for el in self.bot.expansion_locations:
            if self.is_near_expansion(el, self.bot.hq_location):
                print('HQ')
                print(el)
                continue
            else:
                self.bot.open_expansions.append(el)

    def is_near_expansion(self, el, t):
        return t.position.distance_to(el) < self.bot.EXPANSION_GAP_THRESHOLD

    async def manageRaces(self):
        # Detect Player and Enemy Race
        if not self.bot.race_self:
            await self.racecheck_self()
        if not self.bot.race_enemy:
            await self.racecheck_enemy()
        if self.bot.race_enemy == 'Unknown':
            await self.racecheck_guessenemy()

        if not self.bot.worker:
            self.bot.worker = self.bot.techtree.get_id_from_name(self.bot.workers.first.name)
        if not self.bot.townhall:
            self.bot.townhall = self.bot.techtree.get_id_from_name(self.bot.townhalls.first.name)
        if not self.bot.gasbuilding:
            if self.bot.race_self == 'Protoss':
                gasbuildingid = self.bot.techtree.get_id_from_name('Assimilator')
            if self.bot.race_self == 'Terran':
                gasbuildingid = self.bot.techtree.get_id_from_name('Refinery')
            if self.bot.race_self == 'Zerg':
                gasbuildingid = self.bot.techtree.get_id_from_name('Extractor')
            self.bot.gasbuilding = UnitTypeId(gasbuildingid)

        if self.bot.race_self == 'Protoss':
            self.bot.townhalls = self.bot.units(UnitTypeId.NEXUS)
        elif self.bot.race_self == 'Terran':
            self.bot.townhalls = self.bot.units.filter(lambda w: w.type_id == UnitTypeId.COMMANDCENTER or w.type_id == UnitTypeId.ORBITALCOMMAND or w.type_id == UnitTypeId.PLANETARYFORTRESS)
        elif self.bot.race_self == 'Zerg':
            self.bot.townhalls = self.bot.units.filter(lambda w: w.type_id == UnitTypeId.HATCHERY or w.type_id == UnitTypeId.LAIR or w.type_id == UnitTypeId.HIVE)

    def detect_race(self, query):
        for unit in self.unit:
            if unit['name'] == query:
                return unit['race']

    async def racecheck_self(self):
        """detects played race and sets race specific options"""
        self.bot.race_self = str(self.detect_race(self.bot.townhalls.first.name))
        print("Playing as " + str(self.bot.race_self))

    async def racecheck_enemy(self):
        """detects enemy race and sets race specific options"""
        if self.bot.player_id == 1:
            self.bot.enemy_id = 2
        else:
            self.bot.enemy_id = 1
        self.bot.race_enemy = Race(self.bot._game_info.player_races[self.bot.enemy_id])
        if self.bot.race_enemy == Race.Random:
            self.bot.race_enemy = 'Unknown'
        print("Enemy Race is " + str(self.bot.race_enemy))

    async def racecheck_guessenemy(self):
        if self.bot.enemy_units:
            for unit in self.bot.enemy_units:
                if self.detect_race(unit.name):
                    unitrace = self.detect_race(unit.name)
                    self.bot.race_enemy = unitrace
                    print("Enemy Race (Random) is " + str(self.bot.race_enemy))

    async def set_positions(self):
        # HQ
        if not self.bot.hq_location:
            self.bot.hq_location = self.bot.units(self.bot.townhall).ready.first.position
            print("Location: HQ = " + str(self.bot.hq_location))

        # Ramp
        if not self.bot.ramp_location:
            pos = self.bot.main_base_ramp.barracks_in_middle
            nx = self.bot.units(self.bot.townhall).ready.first.position
            p = pos.towards(nx, 4)
            self.bot.ramp_location = p
            print("Location: Ramp = " + str(self.bot.ramp_location))
            #return

        # Rush wait location
        if not self.bot.rushwait_location:
#            self.bot.rushwait_location = await self.bot.get_next_expansion()
            rushwait = await self.bot.get_next_expansion()
#            print(f"Rush: {rushwait}")
            posx = rushwait[0] * .95 + self.bot.map_center.x *.05
            posy = rushwait[1] * .90 + self.bot.map_center.y *.10
            self.bot.rushwait_location = Point2((posx, posy))
            print(f"Location: Rush wait = {str(self.bot.rushwait_location)}")
#            quit()
# test code 0.3.6

        # Proxy Position
        if not self.bot.proxy_location:
            if self.bot.enemy_structures:
                target = self.bot.enemy_structures.first.position
                nx = self.bot.units(self.bot.townhall).ready.first.position
                self.bot.proxy_location = target.towards(nx.position, random.randrange(40, 50))
                print("Location: Proxy Pylon = " + str(self.bot.proxy_location))

    async def mapbounds(self):
        raw_map = self.bot.game_info.playable_area
        map_width = raw_map[2]
        map_height = raw_map[3]
        playerstart = self.bot.game_info.player_start_location

        self.bot.mapcorner_sw = Point2((0, 0))
        self.bot.mapcorner_se = Point2((map_width, 0))
        self.bot.mapcorner_nw = Point2((0, map_height))
        self.bot.mapcorner_ne = Point2((map_width, map_height))
        self.bot.map_center_n = Point2(((map_width / 2), map_height))
        self.bot.map_center_s = Point2(((map_width / 2), 0))
        self.bot.map_center_w = Point2((0, (map_height / 2)))
        self.bot.map_center_e = Point2((map_width, (map_height / 2)))
        self.bot.map_center = Point2(((map_width / 2), (map_height / 2)))

        if playerstart.distance_to(self.bot.mapcorner_sw) < 50:
            self.bot.startpoint = 'SW'
        if playerstart.distance_to(self.bot.mapcorner_se) < 50:
            self.bot.startpoint = 'SE'
        if playerstart.distance_to(self.bot.mapcorner_nw) < 50:
            self.bot.startpoint = 'NW'
        if playerstart.distance_to(self.bot.mapcorner_ne) < 50:
            self.bot.startpoint = 'NE'

        # test 0.3.6
        print(f"{playerstart} in {self.bot.startpoint}")
        # test 0.3.6

    def outofbounds(self, pos):
        if not pos:
            return
        raw_map = self.bot.game_info.playable_area
        map_width = raw_map[2]
        map_height = raw_map[3]

        x = pos[0]
        y = pos[1]

        if (x <= 0) or (x >= map_width):
            return True
        elif (y <= 0) or (y >= map_height):
            return True
