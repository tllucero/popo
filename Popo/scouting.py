import random
import re

from sc2.client import *
from sc2.constants import *
from sc2.units import Units
from sc2.helpers import ControlGroup

from .build_order_id import BuildOrderId


class Scouting():
    def __init__(self, bot=None):
        self.bot = bot

        self.bot.home = None
        self.bot.away = None
        self.o = None
        self.x = None
        self.y = None
        self.target = None
        self.first_overlord_sent = False
        self.second_overlord_sent = False
        self.initial_drone_scout_flag = False
        self.reg_build_done = False
        self.z_scout_time = 0
        self.force_cheese_defense = False
        self.d = None
        self.drones = None
        self.drone_scout_tags = set()
        self.enemy_structure_tags = set()
        self.enemy_townhall_tags = set()

        self.townhalls = [NEXUS, HATCHERY, LAIR, HIVE, COMMANDCENTER,
        PLANETARYFORTRESS, ORBITALCOMMAND]

        self.bot.building_seen = False
        self.bot.ramp_scouted = False
        self.bot.enemy_expansion = False
        self.bot.build_defense_flag = False
        self.bot.roach_rush_flag = False
        self.bot.proxy_rush_flag = False
        self.bot.proxy_pylon = False
        self.bot.cannon_rush = False
        self.bot.attack_power = 0
        self.bot.enemy_power = 0
        self.bot.previous_enemy_power = 0
        self.bot.zergling_rush = False
        self.bot.map_name = ""

        self.bot.all_drone_tags = set()
        self.bot.mineral_drone_tags = set()
        self.bot.vespene_drone_tags = set()
        self.bot.hatchery_drone_tags = set()
        self.bot.building_drone_tags = set()

    def Run(self):
        if self.bot.iteration == 0:
           self.bot.home = self.bot.home.position
           self.bot.away = self.bot.away.position
           self.initial_drone_tags()
           self.set_map_name()
           self.initial_rally_location()

        self.scout()

    def initial_drone_tags(self):
        for d in self.bot.units(DRONE):
            self.bot.mineral_drone_tags.add(d.tag)
            self.bot.all_drone_tags.add(d.tag)

    def set_map_name(self):
        if re.search("Acropolis", self.bot.game_info.map_name):
            self.bot.map_name = "Acropolis"
        elif re.search("Disco Bloodbath", self.bot.game_info.map_name):
            self.bot.map_name = "Disco Bloodbath"
        elif re.search("Ephemeron", self.bot.game_info.map_name):
            self.bot.map_name = "Ephemeron"
        elif re.search("Thunderbird", self.bot.game_info.map_name):
            self.bot.map_name = "Thunderbird"
        elif re.search("Triton", self.bot.game_info.map_name):
            self.bot.map_name = "Triton"
        elif re.search("World of Sleepers", self.bot.game_info.map_name):
            self.bot.map_name = "World of Sleepers"
        elif re.search("Winter's Gate", self.bot.game_info.map_name):
            self.bot.map_name = "Winter's Gate"
        elif re.search("Deathaura", self.bot.game_info.map_name):
            self.bot.map_name = "Deathaura"
        elif re.search("Eternal Empire", self.bot.game_info.map_name):
            self.bot.map_name = "Eternal Empire"
        elif re.search("Ever Dream", self.bot.game_info.map_name):
            self.bot.map_name = "Ever Dream"
        elif re.search("Golden Wall", self.bot.game_info.map_name):
            self.bot.map_name = "Golden Wall"
        elif re.search("Ice and Chrome", self.bot.game_info.map_name):
            self.bot.map_name = "Ice and Chrome"
        elif re.search("Pillars of Gold", self.bot.game_info.map_name):
            self.bot.map_name = "Pillars of Gold"
        elif re.search("Submarine", self.bot.game_info.map_name):
            self.bot.map_name = "Submarine"
        elif re.search("Oxide AIE", self.bot.game_info.map_name):
            self.bot.map_name = "Oxide AIE"
        else:
            self.bot.map_name = "Other Map"

    def initial_rally_location(self):
        if self.bot.map_name == 'Acropolis':
            self.x, self.y = (.026, .318)
        elif self.bot.map_name == 'Disco Bloodbath':
            self.x, self.y = (.13, -0.44)
        elif self.bot.map_name == 'Ephemeron':
            self.x, self.y = (.19, .245)
        elif self.bot.map_name == 'Thunderbird':
            self.x, self.y = (.165, .26)
        elif self.bot.map_name == 'Triton':
            self.x, self.y = (.29, .03)
        elif self.bot.map_name == 'World of Sleepers':
            self.x, self.y = (.08, .27)
        elif self.bot.map_name == "Winter's Gate":
            self.x, self.y = (.10, .30)
        elif self.bot.map_name == 'Deathaura':
            self.x, self.y = (.30, .05)
        elif self.bot.map_name == 'Eternal Empire':
            self.x, self.y = (.11, .26)
        elif self.bot.map_name == 'Ever Dream':
            self.x, self.y = (-.10, .23)
        elif self.bot.map_name == 'Golden Wall':
            self.x, self.y = (.105, .297)
        elif self.bot.map_name == 'Ice and Chrome':
            self.x, self.y = (.05, .27)
        elif self.bot.map_name == 'Pillars of Gold':
            self.x, self.y = (.305, -.02)
        elif self.bot.map_name == 'Submarine':
            self.x, self.y = (.12, .24)
        elif self.bot.map_name == 'Oxide AIE':
            self.x, self.y = (.12, .24)
        else:
            self.x, self.y = (.1, .1)

        self.calculate_location()
        self.bot.rushwait_location = self.target

        for th in self.bot.zerg_towns:
            th(RALLY_HATCHERY_UNITS, self.target)

    def scout(self):
        self.update_tags()
        self.overlord_scout()
        self.drone_scout()
        self.zergling_scout()
        self.war_scout()

    def update_tags(self):
        for d in self.bot.units(DRONE):
            if d.tag not in self.bot.all_drone_tags:
                self.bot.mineral_drone_tags.add(d.tag)
                self.bot.all_drone_tags.add(d.tag)

    def overlord_scout(self):
        if not self.first_overlord_sent and self.bot.time > 0:
            self.first_overlord_scout()
        elif (not self.second_overlord_sent
        and self.bot.units(OVERLORD).amount >= 2):
            self.second_overlord_scout()

    def first_overlord_scout(self):
        self.o = random.choice(self.bot.units(OVERLORD))

        if self.bot.build_order == BuildOrderId.ZVT_ANTI_A_1:
            self.first_overlord_nydus()
            print("nydus scout")
        else:
            self.first_overlord_standard()
            print("standard scout")

        self.move_to_location()
        self.first_overlord_sent = True

    def first_overlord_nydus(self):
        if self.bot.map_name == 'Acropolis':
            self.x, self.y = (.78, 1.03)
        elif self.bot.map_name == 'Disco Bloodbath':
            self.x, self.y = (1.0, .59)
        elif self.bot.map_name == 'Ephemeron':
            self.x, self.y = (.79, 1.04)
        elif self.bot.map_name == 'Thunderbird':
            self.x, self.y = (.78, 1.04)
        elif self.bot.map_name == 'Triton':
            self.x, self.y = (1.04, .78)
        elif self.bot.map_name == "Winter's Gate":
            self.x, self.y = (.70, 1.04)
        elif self.bot.map_name == 'World of Sleepers':
            self.x, self.y = (.79, 1.04)
        elif self.bot.map_name == 'Deathaura':
            self.x, self.y = (.78, 1.04)
        elif self.bot.map_name == 'Eternal Empire':
            self.x, self.y = (.78, 1.04)
        elif self.bot.map_name == 'Ever Dream':
            self.x, self.y = (.78, 1.04)
        elif self.bot.map_name == 'Golden Wall':
            self.x, self.y = (.78, 1.04)
        elif self.bot.map_name == 'Ice and Chrome':
            self.x, self.y = (.78, 1.04)
        elif self.bot.map_name == 'Pillars of Gold':
            self.x, self.y = (.78, 1.04)
        elif self.bot.map_name == 'Submarine':
            self.x, self.y = (.78, 1.04)
        elif self.bot.map_name == 'Oxide AIE':
            self.x, self.y = (.78, 1.04)
        else:
            self.x, self.y = (.78, 1.04)

    def first_overlord_standard(self):
        if self.bot.map_name == 'Acropolis':
            self.x, self.y = (.917, .687)
        elif self.bot.map_name == 'Disco Bloodbath':
            self.x, self.y = (.905, 1.30)
        elif self.bot.map_name == 'Ephemeron':
            self.x, self.y = (.905, .705)
        elif self.bot.map_name == 'Thunderbird':
            self.x, self.y = (.88, .743)
        elif self.bot.map_name == 'Triton':
            self.x, self.y = (.75, .885)
        elif self.bot.map_name == "Winter's Gate":
            self.x, self.y = (.905, .705)
        elif self.bot.map_name == 'World of Sleepers':
            self.x, self.y = (.815, .755)
        elif self.bot.map_name == 'Deathaura':
            self.x, self.y = (.750, 1.05)
        elif self.bot.map_name == 'Eternal Empire':
            self.x, self.y = (.905, .685)
        elif self.bot.map_name == 'Ever Dream':
            self.x, self.y = (1.15, .80)
        elif self.bot.map_name == 'Golden Wall':
            self.x, self.y = (.85, .275)
        elif self.bot.map_name == 'Ice and Chrome':
            self.x, self.y = (1.000, .65)
        elif self.bot.map_name == 'Pillars of Gold':
            self.x, self.y = (.63, 1.065)
        elif self.bot.map_name == 'Submarine':
            self.x, self.y = (.72, .72)
        elif self.bot.map_name == 'Oxide AIE':
            self.x, self.y = (.73, .76)
        else:
            self.x, self.y = (.905, .705)

    def second_overlord_scout(self):
        self.o = self.bot.units(OVERLORD).closest_to(self.bot.hatchery)
        if self.bot.build_order == BuildOrderId.ZVT_ANTI_A_1:
            self.second_overlord_nydus()
        else:
            self.second_overlord_standard()

        self.move_to_location()
        self.second_overlord_sent = True

    def second_overlord_nydus(self):
        if self.bot.map_name == 'Acropolis':
            self.x, self.y = (.917, .687)
        elif self.bot.map_name == 'Disco Bloodbath':
            self.x, self.y = (.905, 1.30)
        elif self.bot.map_name == 'Ephemeron':
            self.x, self.y = (.905, .705)
        elif self.bot.map_name == 'Thunderbird':
            self.x, self.y = (.88, .743)
        elif self.bot.map_name == 'Triton':
            self.x, self.y = (.75, .885)
        elif self.bot.map_name == "Winter's Gate":
            self.x, self.y = (.905, .705)
        elif self.bot.map_name == 'World of Sleepers':
            self.x, self.y = (.815, .755)
        elif self.bot.map_name == 'Deathaura':
            self.x, self.y = (.750, 1.05)
        elif self.bot.map_name == 'Eternal Empire':
            self.x, self.y = (.905, .685)
        elif self.bot.map_name == 'Ever Dream':
            self.x, self.y = (1.15, .80)
        elif self.bot.map_name == 'Golden Wall':
            self.x, self.y = (.85, .275)
        elif self.bot.map_name == 'Ice and Chrome':
            self.x, self.y = (1.000, .68)
        elif self.bot.map_name == 'Pillars of Gold':
            self.x, self.y = (.62, 1.075)
        elif self.bot.map_name == 'Submarine':
            self.x, self.y = (.72, .72)
        elif self.bot.map_name == 'Oxide AIE':
            self.x, self.y = (.73, .76)
        else:
            self.x, self.y = (.905, .705)

    def second_overlord_standard(self):
        if self.bot.map_name == 'Acropolis':
            self.x, self.y = (.083, .313)
        elif self.bot.map_name == 'Disco Bloodbath':
            self.x, self.y = (.15, -0.31)
        elif self.bot.map_name == 'Ephemeron':
            self.x, self.y = (.31, .19)
        elif self.bot.map_name == 'Thunderbird':
            self.x, self.y = (.125, .275)
        elif self.bot.map_name == 'Triton':
            self.x, self.y = (.28, .12)
        elif self.bot.map_name == "Winter's Gate":
            self.x, self.y = (.05, .30)
        elif self.bot.map_name == 'World of Sleepers':
            self.x, self.y = (.10, .27)
        elif self.bot.map_name == 'Deathaura':
            self.x, self.y = (.27, .08)
        elif self.bot.map_name == 'Eternal Empire':
            self.x, self.y = (.10, .25)
        elif self.bot.map_name == 'Ever Dream':
            self.x, self.y = (.07, .29)
        elif self.bot.map_name == 'Golden Wall':
            self.x, self.y = (.16, .275)
        elif self.bot.map_name == 'Ice and Chrome':
            self.x, self.y = (.15, .4)
        elif self.bot.map_name == 'Pillars of Gold':
            self.x, self.y = (.38, -.075)
        elif self.bot.map_name == 'Submarine':
            self.x, self.y = (.28, .28)
        elif self.bot.map_name == 'Oxide AIE':
            self.x, self.y = (.27, .24)
        else:
            self.x, self.y = (.15, .4)

    def move_to_location(self):
        self.calculate_location()
        print('scouting target: ', self.target)
        self.o.move(self.target)

    def calculate_location(self):
        if self.bot.map_name == 'Golden Wall':
            posx = (self.bot.home.x + self.x * (self.bot.away.x -self.bot.home.x))
            posy = (self.bot.home.y + self.y * (self.bot.mapcorner_ne.y -self.bot.home.y))
        else:
            posx = (self.bot.home.x + self.x * (self.bot.away.x -self.bot.home.x))
            posy = (self.bot.home.y + self.y * (self.bot.away.y -self.bot.home.y))

        self.target = Point2((posx, posy))

    def drone_scout(self):
        if self.bot.build_order == BuildOrderId.WORKER_RUSH:
            return
        if ((not self.initial_drone_scout_flag)
            and self.bot.time >= 52):
            self.initial_drone_scout()

    def initial_drone_scout(self):
        self.pick_drone_to_scout()

        if self.drones:
            target = self.bot.away
            self.d = self.drones.closest_to(target)
            self.d.move(target)
            self.update_scouting_drone_tags()
            print('drone scouting target: ', target)
            self.initial_drone_scout_flag = True

    def pick_drone_to_scout(self):
        distance = 10
        self.drones = self.bot.units.filter(lambda d:
            d.type_id == DRONE
            and d.distance_to(self.bot.home) < distance
            and d.tag in self.bot.mineral_drone_tags
            and not d.is_carrying_resource)

    def update_scouting_drone_tags(self):
        self.drone_scout_tags.add(self.d.tag)
        self.bot.mineral_drone_tags.remove(self.d.tag)


    def zergling_scout(self):
        if (self.bot.units(ZERGLING) == 0
        or self.z_scout_time + 10 > self.bot.time):
            return
        if (self.bot.roach_rush_flag and self.bot.time < 600):
            return

        c = 0
        for z in self.bot.units(ZERGLING):
            target = random.choice(self.bot.open_expansions)
            z.attack(target)
            self.z_scout_time = self.bot.time
            c += 1
            if c >= 2:
                break

    def war_scout(self):
        if self.bot.time < 15:
            return

        if (not self.reg_build_done
        and len(self.bot.build_order_list) == 0):
            print(f"\nTime: {self.bot.time_formatted} ", end="")
            print("reg build done")
            self.reg_build_done = True
        self.structure_scout()
        self.self_scout()
        self.enemy_unit_scout()
        if self.bot.race_enemy == Race.Zerg:
            self.roach_rush_scout()
        if self.bot.race_enemy == Race.Protoss:
            self.scout_protoss_cheese()
        if self.bot.race_enemy != Race.Zerg:
            self.scout_wall()
        self.rush_defense_check()

    def structure_scout(self):
        if len(self.bot.enemy_structures) > 0:
            if not self.bot.building_seen:
                print('***********')
                print(f"Time: {self.bot.time_formatted}", end=' ')
            for self.b in self.bot.enemy_structures:
                self.scout_structure()
            self.bot.building_seen = True

    def scout_structure(self):
        if not self.bot.building_seen:
            print(f" Pos: {self.b.position} ", end=" ")
            print(f"Building: {self.b.name} Tag: {self.b.tag} ", end="")
            print(f"Progress: {self.b.build_progress:.4}")
            self.scout_townhall()
            if self.enemy_townhall_tags and self.b.position != self.bot.away:
                self.bot.enemy_expansion = True

        if self.b.tag not in self.enemy_structure_tags:
            self.enemy_structure_tags.add(self.b.tag)
            print(f"Time: {self.bot.time_formatted}", end=" ")
            print(f" Pos: {self.b.position} ", end=" ")
            print(f"{self.b.name} Tag: {self.b.tag}", end=" ")
            print(f"Progress: {self.b.build_progress:.4}")

    def scout_townhall(self):
        if self.b.type_id in (UnitTypeId.NEXUS, UnitTypeId.COMMANDCENTER,
            UnitTypeId.ORBITALCOMMAND, UnitTypeId.PLANETARYFORTRESS,
            UnitTypeId.HATCHERY, UnitTypeId.LAIR, UnitTypeId.HIVE
        ):
            self.enemy_townhall_tags.add(self.b.tag)

    def self_scout(self):
        mineral_cost = 0
        vespene_cost = 0
        if len(self.bot.units) > 0:
            for unit in self.bot.units.filter(lambda w:
                w.type_id not in [UnitTypeId.DRONE, UnitTypeId.OVERLORD,
                UnitTypeId.OVERSEER, UnitTypeId.OVERLORDTRANSPORT,
                UnitTypeId.OVERLORDCOCOON]
            ):
                cost = self.bot.calculate_unit_value(unit.type_id)
                mineral_cost += cost.minerals
                vespene_cost += cost.vespene
        self.bot.attack_power = (mineral_cost + vespene_cost)

    def enemy_unit_scout(self):
        mineral_cost = 0
        vespene_cost = 0
        if len(self.bot.enemy_units.not_structure) > 0:
            for unit in self.bot.enemy_units.not_structure.filter(lambda w:
                w.type_id not in [UnitTypeId.DRONE, UnitTypeId.PROBE,
                    UnitTypeId.SCV, UnitTypeId.OVERLORD, UnitTypeId.OVERSEER,
                    UnitTypeId.OVERLORDTRANSPORT, UnitTypeId.OVERLORDCOCOON]
            ):
                cost = self.bot.calculate_unit_value(unit.type_id)
                mineral_cost += cost.minerals
                vespene_cost += cost.vespene
        self.bot.enemy_power = (mineral_cost + vespene_cost)
        self.bot.enemy_power = max(self.bot.enemy_power, self.bot.previous_enemy_power)
        self.bot.previous_enemy_power = self.bot.enemy_power

    def roach_rush_scout(self):
        if self.bot.time > 180:
            return
        if len(self.bot.enemy_units(UnitTypeId.ROACH)) >= 3:
            if not self.bot.roach_rush_flag:
                self.roach_rush_prep()
                self.bot.roach_rush_flag = True

    def rush_defense_check(self):
        if (self.bot.zergling_rush and self.bot.time < 300):
            return
        elif (self.bot.time < 300 and not self.bot.zergling_rush):
            self.zergling_rush_check()
        elif (self.bot.attack_power < self.bot.enemy_power and
            self.bot.time < 300
        ):
            if not self.bot.build_defense_flag:
                print('Go defense!')
            self.bot.build_defense_flag = True
        else:
            # comment next line before production - protoss cheese
            # if self.bot.time < 300:
                # return
            if self.bot.build_defense_flag:
                print('Back to tested logic.')
            self.bot.build_defense_flag = False

    def zergling_rush_check(self):
        enemy_zerglings = len(self.bot.enemy_units.not_structure(UnitTypeId.ZERGLING))
        if enemy_zerglings > self.bot.time / 18:
            self.bot.zergling_rush = True
            self.bot.build_defense_flag = True
            print("zergling rush detected")
            self.rush_rally_point()

    def roach_rush_prep(self):
        print("roach rush detected")
        self.rush_rally_point()

    def rush_rally_point(self):
        self.x, self.y = (1.01, 1.01)
        posx = (self.bot.home.x + self.x * (self.bot.ramp_location.x -self.bot.home.x))
        posy = (self.bot.home.y + self.y * (self.bot.ramp_location.y -self.bot.home.y))
        self.target = Point2((posx, posy))
        self.bot.rushwait_location = self.target
        print('rally point: ', self.target)

        # for th in self.bot.zerg_towns:
            # print('town rally point: ', self.target)

    def scout_protoss_cheese(self):
        if (self.bot.build_order == BuildOrderId.DEFEND_PROXY_GATEWAY and
            self.reg_build_done
        ):
            self.force_cheese_defense = True

        if self.force_cheese_defense:
            self.force_protoss_cheese_defense()

        if len(self.bot.enemy_structures) == 0:
            return

        self.scout_pylon()
        self.scout_cannon_rush()
        self.proxy_rush()

    def force_protoss_cheese_defense(self):
        # forces cheese defense
        if self.bot.time >= 41 and self.bot.time < 300:
            if not self.bot.proxy_pylon:
                self.bot.proxy_pylon = True
                self.bot.build_defense_flag = True
                print(f"Time: {self.bot.time_formatted}", end=' ')
                print("proxy pylon")
                self.rush_rally_point()
        if self.bot.time >= 57 and self.bot.time < 300:
            if not self.bot.proxy_rush_flag:
                self.bot.proxy_rush_flag = True
                self.bot.build_defense_flag = True
                print(f"Time: {self.bot.time_formatted}", end=' ')
                print("proxy gateway")

    def scout_pylon(self):
        if self.bot.time >= 120 or self.bot.proxy_pylon:
            return
        for e in self.bot.enemy_structures(PYLON):
            if e.position.distance_to(self.bot.home) < 70:
                self.bot.proxy_pylon = True
                self.bot.build_defense_flag = True
                print(f"Time: {self.bot.time_formatted}", end=' ')
                print("proxy pylon")
                self.rush_rally_point()

    def scout_cannon_rush(self):
        if (self.bot.time >= 120 or self.bot.cannon_rush or
            not self.bot.proxy_pylon
        ):
            return

        enemy_structures = self.bot.enemy_structures.filter(lambda w:
            w.type_id in [UnitTypeId.FORGE]
            or w.type_id in [UnitTypeId.PHOTONCANNON])

        if len(enemy_structures) > 0:
            self.cannon_rush_prep()

    def cannon_rush_prep(self):
        self.bot.cannon_rush = True
        self.bot.build_defense_flag = True
        print(f"Time: {self.bot.time_formatted}", end=' ')
        print("cannon rush")
        self.rush_rally_point()

    def proxy_rush(self):
        if self.bot.time >= 120 or self.bot.proxy_rush_flag:
            return
        for e in self.bot.enemy_structures(GATEWAY):
            if e.position.distance_to(self.bot.home) < 70:
                self.bot.build_defense_flag = True
                self.bot.proxy_rush_flag = True
                print(f"Time: {self.bot.time_formatted}", end=' ')
                print("proxy gateway")

    def scout_wall(self):
        if (self.bot.time >= 105 and not
            self.bot.building_seen and not
            self.bot.ramp_scouted
        ):
            self.overlord_scout_ramp()
            self.bot.ramp_scouted = True

    def overlord_scout_ramp(self):
        self.o = self.bot.units(OVERLORD).closest_to(self.bot.away)
        print("scout ramp")
        self.scout_enemy_ramp_location()
        self.move_to_location()

    def scout_enemy_ramp_location(self):
        if self.bot.map_name == 'Deathaura':
            self.x, self.y = (.83, .96)
        elif self.bot.map_name == 'Eternal Empire':
            self.x, self.y = (.95, .78)
        elif self.bot.map_name == 'Ever Dream':
            self.x, self.y = (1.115, .805)
        elif self.bot.map_name == 'Golden Wall':
            self.x, self.y = (.89, .22)
        elif self.bot.map_name == 'Ice and Chrome':
            self.x, self.y = (1.005, .785)
        elif self.bot.map_name == 'Pillars of Gold':
            self.x, self.y = (.78, 1.075)
        elif self.bot.map_name == 'Submarine':
            self.x, self.y = (.85, .80)
        elif self.bot.map_name == 'Oxide AIE':
            self.x, self.y = (.85, .80)
        else:
            self.x, self.y = (.905, .85)
