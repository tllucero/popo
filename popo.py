"""Popo.py plays StarCraft 2 as a zerg AI.
The AI is not built in, but runs off data collected
from ai-arena.net.
"""
from functools import reduce
from operator import or_
import random

import sc2
from sc2.constants import *
from sc2.player import Bot
from sc2.data import race_townhalls
from sc2.client import *

from techtree import *
from Popo.gamemanager import *
from Popo.resources import *
from Popo.scouting import *
from Popo.warmanager import *
from Popo.buildorder import *
from Popo.writedata import *

class Popo(sc2.BotAI):
    """Popo is the main program for the StarCraft 2 zerg bot.
    """

    def __init__(self):
        super().__init__()
        self.techtree = TechTree(self)
        self.resourcess = Resourcess(self)
        self.gamemanager = GameManager(self)
        self.scouting = Scouting(self)
        self.warmanager = WarManager(self)
        self.buildorder = BuildOrder(self)
        self.write_data = WriteData(self)
        self.iteration = 0
        self.NAME = "Popo-0.4.7.0"
        self.production_version = True


        # Popo starting point
        self.result = None
        self.expected_result = 1  # 1 win 0 tie -1 loss
        self.win = 1
        self.tie = 0
        self.loss = -1
        self.opponent_name = ''
        self.previous_structure_count = 1
        self.zerg_towns = None
        self.hatcheries = None
        self.zerg_towns_ready = None
        self.previous_towns = 0
        self.queen_started = False
        self.build_order = None
        self.next_hatchery_position = None
        self.worker_for_hatchery = None
        self.worker_moving_to_hatchery = False
        self.minerals_previous = 0

    async def on_step(self, iteration):
        if iteration == 0:
            await self.gamemanager.startGame()
            await self.chat_send(f"Name: {self.NAME}")
            await self.chat_send("(glhf)")
            self.opponent_id_name()
            self.write_data.print_start()

        self.iteration = iteration
        if ((abs(self.minerals - self.minerals_previous) >= 50
            and self.time <= 60) or self.time % 15 < 0.15
        ):
            print(f"Time: {self.time_formatted} ", end="")
            print(f"min {self.minerals} gas {self.vespene} ", end="")
            print(f"supply {self.supply_used} / {self.supply_cap} ", end="")
            print(f"larva: {len(self.units(LARVA))} ", end="")
            print(f"Attack: {self.attack_power} ", end="")
            print(f"Enemy: {self.enemy_power}")
            self.minerals_previous = self.minerals

        # Manage own and enemy Race
        await self.gamemanager.manageRaces()

        self.get_game_status()
        self.get_townhalls()
        self.get_hatcheries()
        self.get_zerg_towns_ready()
        if iteration == 0:
            self.home = self.hatcheries.first.position
            self.away = self.enemy_start_locations[0].position
            print(f"Home: {self.home}")
            print(f"Away: {self.away}")
            print(f"Name: {self.NAME}")
            print(f"Opponent: {self.opponent_id} ")
            print(f"Opponent: {self.opponent_name} ")
            print(f"Map: {self.game_info.map_name}")
            self.initialize_extractors()
            self.write_data.print_opponent_info()

        self.larvae = self.units(LARVA)
        self.scouting.Run()
        await self.buildorder.Run()
        ### For local only
        if iteration == 0:
            if self.production_version:
                await self.chat_send("Rolled a natural 20. Better luck next game.")
            else:
                await self.chat_send(f"Build order: {self.build_order}")
        ### For local only
        await self.resourcess.Run()

        self.warmanager.iteration = self.iteration
        await self.warmanager.Run()
        # await self.one_base_broodlord()

    async def on_end(self, game_result):
        print("Game ended.")
        print(game_result)
        self.write_data.print_result(game_result)
        self.write_data.print_end()

    def get_game_status(self):
        """Guess whether bot is in a tie game or a loss.
        If the game looks like a tie or a loss, write a message
        to the game file in the data directory.
        If no message, that usually (but not always) means a win.
        """
        structure_count = len(self.structures)
        if self.time > (60 * 59 + 30):
            if self.expected_result != self.tie:
                self.write_data.print_tie()
                self.expected_result = self.tie
        elif (structure_count < 3 and self.previous_structure_count >= 3
        ) or len(self.units) < 3:
            if self.expected_result != self.loss:
                self.write_data.print_loss()
                self.expected_result = self.loss

        self.previous_structure_count = structure_count

    def opponent_id_name(self):
        # comment this out before production
        # self.production_version = False
        if not self.production_version:
            self.opponent_id = '1574858b-d54f-47a4-b06a-0a6431a61ce9'

        if self.opponent_id == 'ba7782ea-4dde-4a25-9953-6d5587a6bdcd':
            self.opponent_name = 'AdditionalPylons'
        elif self.opponent_id == 'd7bd5012-d526-4b0a-b63a-f8314115f101':
            self.opponent_name = 'ANIbot'
        elif self.opponent_id == 'ec603d06-e2d7-4c77-a0ce-690931375b56':
            self.opponent_name = 'BaronessZuli'
        elif self.opponent_id == '2557ad1d-ee42-4aaa-aa1b-1b46d31153d2':
            self.opponent_name = 'BenBotBC'
        elif self.opponent_id == 'b7c17894-8f38-423b-87d2-f983065364f3':
            self.opponent_name = 'BenBotv3'
        elif self.opponent_id == '5d8dceae-5f36-4e9b-81ba-6a20fd8977e6':
            self.opponent_name = 'BetterWorkerRush'
        elif self.opponent_id == '7b8f5f78-6ca2-4079-b7c0-c7a3b06036c6':
            self.opponent_name = 'BlinkerBot'
        elif self.opponent_id == 'e7d5b34c-d455-432a-8a90-1f08e2065c42':
            self.opponent_name = 'Blunty'
        elif self.opponent_id == 'a7b9a217-919d-46d5-b214-3fea6e92b15c':
            self.opponent_name = 'Bombshaker'
        elif self.opponent_id == '2994c8ff-7cae-4498-8390-49cf3d8c1d82':
            self.opponent_name = 'BraxBot'
        elif self.opponent_id == 'b01e84e6-cfcd-4951-b5af-089c73ec8cc2':
            self.opponent_name = 'Chance'
        elif self.opponent_id == '76cc9871-f9fb-4fc7-9165-d5b748f2734a':
            self.opponent_name = 'dantheman_3'
        elif self.opponent_id == 'ba9de5a0-be16-4f47-aa90-7629e0e57c80':
            self.opponent_name = 'Derg'
        elif self.opponent_id == 'a5d14039-6f6e-4df9-be5f-721ec4ee27ea':
            self.opponent_name = 'DoogieHowitzer'
        elif self.opponent_id == 'bff29cf8-214c-4ac4-a491-a4a03ad11471':
            self.opponent_name = 'dronedronedrone'
        elif self.opponent_id == 'e10f2ba3-f345-428f-b627-2658036511b1':
            self.opponent_name = 'EmptySeat'
        elif self.opponent_id == '4663c2dc-1250-47ee-b1ef-534cad74651d':
            self.opponent_name = 'Excess1972'
        elif self.opponent_id == '3c78e739-5bc8-4b8b-b760-6dca0a88b33b':
            self.opponent_name = 'Fidolina'
        elif self.opponent_id == 'c25e8cb3-1baf-42d7-b744-4be6ba28a5f5':
            self.opponent_name = 'Fire'
        elif self.opponent_id ==  'ca07071b-d8d4-4bc0-a1cd-a1ddda197631':
            self.opponent_name = 'FourGateBot'
        elif self.opponent_id == '54bca4a3-7539-4364-b84b-e918784b488a':
            self.opponent_name = 'Jensiibot'
        elif self.opponent_id == 'eed44128-f488-4e31-b457-8e55f8a95628':
            self.opponent_name = 'Kagamine'
        elif self.opponent_id == 'af261065-9a71-4958-9f4e-fe278b3e17ac':
            self.opponent_name = 'KAI'
        elif self.opponent_id == '6ddf718d-07ec-4c41-9ee8-14c469533ffb':
            self.opponent_name = 'Ketroc'
        elif self.opponent_id == 'aa7b97b6-6beb-48a6-953e-a8aa28490f49':
            self.opponent_name = 'LamaGate'
        elif self.opponent_id == '806ece42-ced6-434c-8edf-294d0b02597b':
            self.opponent_name = 'LucidZJS'
        elif self.opponent_id == '9bd53605-334c-4f1c-95a8-4a735aae1f2d':
            self.opponent_name = 'MadAI'
        elif self.opponent_id == '81fa0acc-93ea-479c-9ba5-08ae63b9e3f5':
            self.opponent_name = 'Micromachine'
        elif self.opponent_id == '2971a2d8-3210-4348-829c-4e59d596d5c7':
            self.opponent_name = 'NewBy'
        elif self.opponent_id == '219e4b0e-dcce-493b-ae80-302cbcb8af73':
            self.opponent_name = 'NikbotLearns'
        elif self.opponent_id == '73ae0017-c268-4920-b4e8-74c57f26e358':
            self.opponent_name = 'NikbotZerg'
        elif self.opponent_id == '11f8230d-e131-41be-9944-b15bd1dd02f7':
            self.opponent_name = 'Noobgam2'
        elif self.opponent_id == 'a9919c1a-7c73-45c9-915e-fa20c3280f39':
            self.opponent_name = 'Paul'
        elif self.opponent_id == 'c8ed3d8b-3607-40e3-b7fe-075d9c08a5fd':
            self.opponent_name = 'QueenBot'
        elif self.opponent_id == 'e8972ce3-c84b-4269-9a94-d76fc94aa2bc':
            self.opponent_name = 'RookieBot'
        elif self.opponent_id == '496ce221-f561-42c3-af4b-d3da4490c46e':
            self.opponent_name = 'RStrelok'
        elif self.opponent_id == '4eb61a21-d2ac-44c8-9087-2654b688bfcc':
            self.opponent_name = 'Rusty'
        elif self.opponent_id == '2540c0f3-238f-40a7-9c39-2e4f3dca2e2f':
            self.opponent_name = 'sharkbot'
        elif self.opponent_id == 'c5e0e203-bfa8-4f8f-a96d-5235a9a481af':
            self.opponent_name = 'SharpenedEdge'
        elif self.opponent_id == '16ab8b85-cf8b-4872-bd8d-ebddacb944a5':
            self.opponent_name = 'sharpy_PVP_EZ'
        elif self.opponent_id == '9bcd0618-172f-4c70-8851-3807850b45a0':
            self.opponent_name = 'Snowbot'
        elif self.opponent_id == '0da37654-1879-4b70-8088-e9d39c176f19':
            self.opponent_name = 'Spiny'
        elif self.opponent_id == '1574858b-d54f-47a4-b06a-0a6431a61ce9':
            self.opponent_name = 'Sproutch'
        elif self.opponent_id == '5714a116-b8c8-42f5-b8dc-93b28f4adf2d':
            self.opponent_name = 'spudde'
        elif self.opponent_id == '0280d5b3-6e58-4cb3-8963-35bd1f3378f6':
            self.opponent_name = 'SunTzuBot'
        elif self.opponent_id == '30935ec2-f10b-4a74-a0f7-9bcaf713d46a':
            self.opponent_name = 'TheGoldenArmada'
        elif self.opponent_id == 'da677994-8e56-4fb8-ac89-19c2e870d3f5':
            self.opponent_name = 'TheHarvester'
        elif self.opponent_id == 'b4d7dc43-3237-446f-bed1-bceae0868e89':
            self.opponent_name = 'ThreeWayLover'
        elif self.opponent_id == '1bd341ea-9ac9-4b6a-866d-67de38bb124d':
            self.opponent_name = 'Tyr'
        elif self.opponent_id == 'e7120c68-f3ee-4772-b20e-c877c8363b8c':
            self.opponent_name = 'TyrP'
        elif self.opponent_id == 'aad42273-84e0-4ae8-8c68-366951ac5c5e':
            self.opponent_name = 'TyrT'
        elif self.opponent_id == '3f36d563-c588-4c08-ad84-222a0456535d':
            self.opponent_name = 'TyrZ'
        elif self.opponent_id == '93f022aa-5d22-411c-92a0-3dd3d44ec33b':
            self.opponent_name = 'WizardHat'
        elif self.opponent_id == '12c39b76-7830-4c1f-9faa-37c68183396b':
            self.opponent_name = 'WorthlessBot'
        elif self.opponent_id == 'fb7813da-a1d6-417f-b271-e55ac26845b3':
            self.opponent_name = 'Zerg001'
        elif self.opponent_id == '81fc0287-0e6d-4db9-9bf7-8e7dfdf44809':
            self.opponent_name = 'Zidolina'
        else:
            self.opponent_name = 'unknown'

    def get_townhalls(self):
        # this includes pending structures
        self.zerg_towns = []
        for s in self.structures:
            if (s.type_id == UnitTypeId.HATCHERY
                or s.type_id == UnitTypeId.LAIR
                or s.type_id == UnitTypeId.HIVE
            ):
                self.zerg_towns.append(s)

        if self.previous_towns != len(self.zerg_towns):
            print(f"Zerg towns: {len(self.zerg_towns)}")
            self.previous_towns = len(self.zerg_towns)

    def get_hatcheries(self):
        """Returns ready hatcheries, but not lairs or hives
        """
        # only hatcheries, only ready
        self.hatcheries = self.structures.filter(
            lambda structure: structure.type_id == UnitTypeId.HATCHERY
            and structure.is_ready
        )
        # if self.hatcheries.amount > 0:
            # self.hatchery = self.hatcheries.first
        # else:
            # self.hatchery = None

    def get_zerg_towns_ready(self):
        """Returns ready hatcheries, lairs, and hives."""
        self.zerg_towns_ready = self.structures.filter(lambda structure:
            structure.type_id == UnitTypeId.HATCHERY and structure.is_ready or
            structure.type_id == UnitTypeId.LAIR or
            structure.type_id == UnitTypeId.HIVE
        )
        if self.zerg_towns_ready.amount > 0:
            self.hatchery = self.zerg_towns_ready.first
        else:
            self.hatchery = None

    def initialize_extractors(self):
        vespenes = self.vespene_geyser.closer_than(14.0, self.hatchery.position)
        for vespene in vespenes:
            print(f"Extractor position: {vespene.position}")

# code from here to be scrapped as convenient - not used`

    async def one_base_broodlord(self):
        larvae = self.larvae
        iteration = self.iteration
        forces = self.units(ZERGLING) | self.units(CORRUPTOR) | self.units(BROODLORD)
        # actions = []

        if self.units(BROODLORD).amount > 2 and iteration % 50 == 0:
            for unit in forces:
                unit.attack(self.select_target())

        if self.supply_left < 2:
            if self.can_afford(OVERLORD) and larvae.exists:
                self.larva.random.train((OVERLORD),
                    subtract_cost=True, subtract_supply=True)
                return

        corruptors = self.units(CORRUPTOR)
        if self.structures(GREATERSPIRE).ready.exists:
            # build half-and-half corruptors and broodlords
            if corruptors.exists and corruptors.amount > self.units(BROODLORD).amount:
                if self.can_afford(BROODLORD):
                    corruptors.random.train(BROODLORD)
            elif self.can_afford(CORRUPTOR) and larvae.exists:
                if corruptors.amount < self.units(BROODLORD).amount + 5:
                    larvae.random.train(CORRUPTOR)
                    return
        elif self.already_pending(GREATERSPIRE):
            if self.can_afford(CORRUPTOR) and larvae.exists:
                larvae.random.train(CORRUPTOR)
                return

        if not self.townhalls.exists:
            for unit in self.units(DRONE) | self.units(QUEEN) | forces:
                unit.attack(self.enemy_start_locations[0])
            return
        else:
            hq = self.townhalls.first

        for queen in self.units(QUEEN).idle:
            abilities = await self.get_available_abilities(queen)
            if AbilityId.EFFECT_INJECTLARVA in abilities:
                queen(EFFECT_INJECTLARVA, hq)

        if (self.already_pending(UnitTypeId.SPAWNINGPOOL) +
                self.structures.filter(
                    lambda structure: structure.type_id == UnitTypeId.SPAWNINGPOOL
                    and structure.is_ready).amount == 0):
            if self.can_afford(SPAWNINGPOOL):
                await self.build(SPAWNINGPOOL, near=hq)

        if self.structures(SPAWNINGPOOL).ready.exists:
            if not self.units(LAIR).exists and not self.units(HIVE).exists and hq.is_idle:
                if self.can_afford(LAIR):
                    hq.build(LAIR)

        if self.structures(LAIR).ready.exists:
            if not (self.structures(INFESTATIONPIT).exists or self.already_pending(INFESTATIONPIT)):
                if self.can_afford(INFESTATIONPIT):
                    await self.build(INFESTATIONPIT, near=hq)

            if not (self.structures(SPIRE).exists or self.already_pending(SPIRE)):
                if self.can_afford(SPIRE):
                    await self.build(SPIRE, near=hq)

        if (self.structures(INFESTATIONPIT).ready.exists
                and not self.units(HIVE).exists and hq.is_idle):
            if self.can_afford(HIVE):
                hq.build(HIVE)

        if self.townhalls(HIVE).ready.exists:
            spires = self.structures(SPIRE).ready
            if spires.exists:
                spire = spires.random
                if self.can_afford(GREATERSPIRE) and spire.is_idle:
                    spire.build(GREATERSPIRE)

        if self.gas_buildings.amount < 2 and not self.already_pending(EXTRACTOR):
            if self.can_afford(EXTRACTOR):
                drone = self.workers.random
                target = self.vespene_geyser.closest_to(drone.position)
                drone.build(EXTRACTOR, target)

        if hq.assigned_harvesters < hq.ideal_harvesters:
            if self.can_afford(DRONE) and larvae.exists:
                larva = larvae.random
                self.larva.random((AbilityId.LARVATRAIN_DRONE),
                        subtract_cost=True, subtract_supply=True)
                return

        for a in self.gas_buildings:
            if a.assigned_harvesters < a.ideal_harvesters:
                w = self.workers.closer_than(20, a)
                if w.exists:
                    w.random.gather(a)

        if self.structures(SPAWNINGPOOL).ready.exists:
            if not self.units(QUEEN).exists and hq.is_ready and hq.is_idle:
                if self.can_afford(QUEEN):
                    hq.train(QUEEN)

        if self.units(ZERGLING).amount < 40 and self.minerals > 1000:
            if larvae.exists and self.can_afford(ZERGLING):
                self.larva.random.train((ZERGLING),
                        subtract_cost=True, subtract_supply=True)

    def select_target(self):
        if self.enemy_structures.exists:
            return random.choice(self.enemy_structures).position

        return self.enemy_start_locations[0]
