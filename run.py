import random
import time

import sc2, sys
from ladder import run_ladder_game
from sc2 import Race, Difficulty, AIBuild
from sc2.player import Bot, Computer

from popo import Popo
bot = Bot(Race.Zerg, Popo())

#allmaps = ['AutomatonLE', 'CyberForestLE', 'KairosJunctionLE', 'KingsCoveLE',
#'NewRepugnancyLE', 'PortAleksanderLE', 'YearZeroLE'] # all maps
# allmaps = ['AcropolisLE', 'DiscoBloodbathLE', 'EphemeronLE', 'ThunderbirdLE',
# 'TritonLE', 'WorldofSleepersLE', 'WintersGateLE']  #all maps
#allmaps = ['EphemeronLE', 'EternalEmpireLE', 'NightshadeLE', 'SImulacrumLE',
#'TritonLE', 'WorldofSleepersLE', 'ZenLE']  #all maps
#allmaps = ['DeathAuraLE', 'EternalEmpireLE', 'EverDreamLE',
#    'GoldenWallLE', 'IceandChromeLE', 'PillarsofGoldLE', 'SubmarineLE']

# allmaps = ['DeathAura505', 'EternalEmpire505', 'EverDream505',
#    'GoldenWall505', 'IceandChrome505', 'PillarsofGold505', 'Submarine505']
allmaps = ['2000AtmospheresAIE', 'BlackburnAIE', 'JagannathaAIE',
    'LightshadeAIE', 'OxideAIE', 'RomanticideAIE']

# allmaps = []

Difficulties = [ Difficulty.Medium, Difficulty.MediumHard,
    Difficulty.Hard, Difficulty.Harder, Difficulty.VeryHard,
    Difficulty.CheatVision, Difficulty.CheatMoney, Difficulty.CheatInsane]
_difficulty = Difficulty.VeryHard

_opponent = random.choice([Race.Zerg, Race.Terran, Race.Protoss, Race.Random])

_build = random.choice([AIBuild.RandomBuild, AIBuild.Rush,
    AIBuild.Timing, AIBuild.Power, AIBuild.Macro, AIBuild.Air])


# Uncomment one of these for a specific race
# _opponent = Race.Zerg
# _opponent = Race.Protoss
# _opponent = Race.Terran

## Uncomment one of these for a specific map
# allmaps = ['2000AtmospheresAIE']
# allmaps = ['BlackburnAIE']
# allmaps = ['JagannathaAIE']
# allmaps = ['LightshadeAIE']
# allmaps = ['OxideAIE']
# allmaps = ['RomanticideAIE']
# allmaps = [random.choice(allmaps)]
# allmaps = [allmaps[3]]
# allmaps = ['AbyssalReefLE']

# Uncomment this for random map
# allmaps = [random.choice(allmaps)]

_map = [random.choice(allmaps)][0]

# Uncomment one of these for a specific build
# _build = AIBuild.RandomBuild
# _build = AIBuild.Rush
# _build = AIBuild.Timing
# _build = AIBuild.Power
# _build = AIBuild.Macro
# _build = AIBuild.Air

file_dir='/opt/programming/overmind/local/replays/Popo/0.4.7.0/'
file_name = 'ZZB_' + _map + '_' + time.strftime("%d-%H-%M")+'.SC2Replay'
_save_replay_as = file_dir + file_name

# Start game
if __name__ == '__main__':
    if "--LadderServer" in sys.argv:
        # Ladder game started by LadderManager
        print("Starting ladder game...")
        run_ladder_game(bot)
    else:
        # Local game
        print("Starting local game...")
        sc2.run_game(sc2.maps.get(_map), [
            bot,
            Computer(_opponent, _difficulty, ai_build=_build)
        ], realtime=False, step_time_limit=2.0, game_time_limit=(60*20),
    save_replay_as=_save_replay_as)
